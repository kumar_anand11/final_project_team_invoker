/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.Date;

/**
 *
 * @author putka
 */
public class Employee {
    
    private String firstName;
    private String lastName;
    private int id;
    private static int count = 1;
    private String department;
    private String specialization;
    private String email;
    private String phone;
    private Date dateofJoining;
    private String designation;
    private String hospitalAssignment;
    private String empNo;
    private AccountDetails accountDetl;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }
        
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDateofJoining() {
        return dateofJoining;
    }

    public void setDateofJoining(Date dateofJoining) {
        this.dateofJoining = dateofJoining;
    }

    public String getHospitalAssignment() {
        return hospitalAssignment;
    }

    public void setHospitalAssignment(String hospitalAssignment) {
        this.hospitalAssignment = hospitalAssignment;
    }

    public AccountDetails getAccountDetl() {
        return accountDetl;
    }

    public void setAccountDetl(AccountDetails accountDetl) {
        this.accountDetl = accountDetl;
    }
            
    public Employee() {
        id = count;
        count++;
    }

    public int getId() {
        return id;
    }
   @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
