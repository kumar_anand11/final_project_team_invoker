/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;
import java.util.Date;


public class EmployeeDirectory {
    
    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }
//    public Employee createEmployee(String name)
//    {
//        Employee employee = new Employee();
//        return employee;
//    }
//    
    
    public Employee createEmployee(String firstName, String lastName, String empNo, String specialization, String dept, String email, String phone, Date doj, String designation)
    {
        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmpNo(empNo);
        employee.setDesignation(designation);
        employee.setDateofJoining(doj);
        employee.setDepartment(dept);
        employee.setSpecialization(specialization);
        employee.setEmail(email);
        employee.setPhone(phone);
        employeeList.add(employee);
        return employee;
    }
    public Employee createEmployee(String firstName, String lastName, String empNo, String specialization, String dept, String email, String phone, Date doj, String designation, AccountDetails acc)
    {
        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmpNo(empNo);
        employee.setDesignation(designation);
        employee.setDateofJoining(doj);
        employee.setDepartment(dept);
        employee.setSpecialization(specialization);
        employee.setEmail(email);
        employee.setPhone(phone);
        employee.setAccountDetl(acc);
        employeeList.add(employee);
        return employee;
    }

   
}