/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.AgentRole;
import java.util.ArrayList;

/**
 *
 * @author putka
 */
public class AgentOrganization extends Organization{
    
    public AgentOrganization()
    {
        super(Type.Agent.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> irole = new ArrayList();
        irole.add(new AgentRole());
        return irole;
    }
    
}
