/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.InsuranceAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author putka
 */
public class InsuranceAdminOrganization extends Organization
{
    
    public InsuranceAdminOrganization()
    {
        super(Type.InsuranceAdmin.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> irole = new ArrayList();
        irole.add(new InsuranceAdminRole());
        return irole;
    }
    
}
