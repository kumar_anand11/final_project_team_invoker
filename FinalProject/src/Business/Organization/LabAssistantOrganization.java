/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.LabAssistantRole;
import java.util.ArrayList;

/**
 *
 * @author putka
 */
public class LabAssistantOrganization extends Organization
{
    public LabAssistantOrganization()
    {
        super(Type.LabAssistant.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> hroles = new ArrayList<>();
        hroles.add(new LabAssistantRole());
        return hroles;
    }
}
