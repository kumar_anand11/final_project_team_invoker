/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;



import java.util.ArrayList;
import Business.Organization.Organization.Type;

/**
 *
 * @author putka
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organization = new DoctorOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.LabAssistant.getValue())){
            organization = new LabAssistantOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.DoctorAssistant.getValue()))
        {
            organization = new DoctorAssistantOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.HospitalAdmin.getValue()))
        {
            organization = new HospitalAdminOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Pharmacy.getValue()))
        {
            organization = new PharmacyOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.InsuranceAdmin.getValue())){
            organization = new InsuranceAdminOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Agent.getValue())){
            organization = new AgentOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Supervisor.getValue()))
        {
            organization = new SupervisorOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.Auditor.getValue()))
        {
            organization = new AuditorOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}
