/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.PharmacistRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author putka
 */
public class PharmacyOrganization extends Organization{

    public PharmacyOrganization() 
    {
        super(Type.Pharmacy.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> hroles = new ArrayList();
        hroles.add(new PharmacistRole());
        return hroles;
    }

    
    
}
