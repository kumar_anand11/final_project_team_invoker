/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.AgentOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Agent.AgentWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author putka
 */
public class AgentRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new AgentWorkAreaJPanel(userProcessContainer, account, (AgentOrganization)organization, enterprise, business);
    }
    
}
