/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.SupervisorOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.Supervisor.SupervisorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author putka
 */
public class SupervisorRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new SupervisorWorkAreaJPanel(userProcessContainer, account, (SupervisorOrganization)organization, enterprise, business);
    }
    
}
