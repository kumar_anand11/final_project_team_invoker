/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Shashank
 */

//Class to store the fields of Address section
public class Address {
    
    //Attribute/Variable declaration
    private String streetAddr;
    private String city;
    private String state;
    private String zipCode;

    //Getters and Setters
    public String getStreetAddr() {
        return streetAddr;
    }

    public void setStreetAddr(String streetAddr) {
        this.streetAddr = streetAddr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    //Parametrized Constructor to set values of attributes
    public Address(String streetAddr, String city, String state, String zipCode) {
        this.streetAddr = streetAddr;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }
    
    @Override
    public String toString(){
        return "["+streetAddr+" "+
                    city+" "+
                    state+" "+
                    zipCode+" ]";
    }
   
}
