/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author Shashank
 */
public class HospitalWorkRequest extends WorkRequest{
    public static int count = 0;
    private String visitID;
    private String hospitalID;
    private String doctorID;
    private String doctorAssistantID;
    private String labID;
    private String labStatus;
    private String diagnoses;
    private String labTests;
    private String labResult;
    private String filePath;
    private Patient patient;
    private Date date;
    private Medication medication;
    private double totalBill;
    private double tax;
    private double totalWithTax;
    private double medicationAmt;
    private double labFees;
    private double docFees;
    private String claimID;
    
    //Constructor
    public HospitalWorkRequest() {
        count++;
        visitID="V_"+count;
    }

    public String getLabStatus() {
        return labStatus;
    }

    public void setLabStatus(String labStatus) {
        this.labStatus = labStatus;
    }
    
    
    
    public Date getDate() {
        return date;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
    //Getters and Setters
    public void setDate(Date date) {
        this.date = date;
    }

    public String getVisitID() {
        return visitID;
    }

    public void setVisitID(String visitID) {
        this.visitID = visitID;
    }
    
    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
    
    public String getHospitalID() {
        return hospitalID;
    }

    public void setHospitalID(String hospitalID) {
        this.hospitalID = hospitalID;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getLabID() {
        return labID;
    }

    public void setLabID(String labID) {
        this.labID = labID;
    }

    public String getDiagnoses() {
        return diagnoses;
    }

    public void setDiagnoses(String diagnoses) {
        this.diagnoses = diagnoses;
    }

    public String getLabTests() {
        return labTests;
    }

    public void setLabTests(String labTests) {
        this.labTests = labTests;
    }

    public String getLabResult() {
        return labResult;
    }

    public void setLabResult(String labResult) {    
        this.labResult = labResult;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDoctorAssistantID() {
        return doctorAssistantID;
    }

    public void setDoctorAssistantID(String doctorAssistantID) {
        this.doctorAssistantID = doctorAssistantID;
    } 

    public double getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(double totalBill) {
        this.totalBill = totalBill;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotalWithTax() {
        return totalWithTax;
    }

    public void setTotalWithTax(double totalWithTax) {
        this.totalWithTax = totalWithTax;
    }

    public double getLabFees() {
        return labFees;
    }

    public void setLabFees(double labFees) {
        this.labFees = labFees;
    }

    public double getMedicationAmt() {
        return medicationAmt;
    }

    public void setMedicationAmt(double medicationAmt) {
        this.medicationAmt = medicationAmt;
    }

    public String getClaimID() {
        return claimID;
    }

    public void setClaimID(String claimID) {
        this.claimID = claimID;
    }

    public double getDocFees() {
        return docFees;
    }

    public void setDocFees(double docFees) {
        this.docFees = docFees;
    }
    
    @Override
    public String toString(){
        return "["+hospitalID+", "+
        doctorID+", "+
        doctorAssistantID+", "+
        labID+", "+
        diagnoses+", "+
        labTests+", "+
        labResult+", "+
        patient+", "+
        String.valueOf(totalBill)+", "+
        String.valueOf(tax)+", "+
        String.valueOf(totalWithTax)+", "+
        String.valueOf(medicationAmt)+", "+
        String.valueOf(labFees)+"]";
    }

}
