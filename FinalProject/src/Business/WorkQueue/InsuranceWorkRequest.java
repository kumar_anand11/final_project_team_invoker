/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Shashank
 */
public class InsuranceWorkRequest extends WorkRequest{
    public static int count = 0;
    private String insuranceCompID;
    private String claimID;
    private double claimAmnt;
    private String claimApprvAmnt;
    private String agentID;
    private String supervisorID;
    private String auditorID;
    private HospitalWorkRequest hospitalReq;
    private Policy policy;
    private ArrayList<Remark> remarkList;
    
    //Constructor
    public InsuranceWorkRequest() {
        count++;
        claimID="Claim_"+count;
        remarkList=new ArrayList<>();
    }
    
    //Gettters and Setters
    
    public Policy getPolicy() {
        return policy;
    }    
    public void setPolicy(Policy policy) {    
        this.policy = policy;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        InsuranceWorkRequest.count = count;
    }

    public double getClaimAmnt() {
        return claimAmnt;
    }

    public void setClaimAmnt(double claimAmnt) {
        this.claimAmnt = claimAmnt;
    }

    public String getClaimApprvAmnt() {
        return claimApprvAmnt;
    }

    public void setClaimApprvAmnt(String claimApprvAmnt) {
        this.claimApprvAmnt = claimApprvAmnt;
    }
   
    public String getInsuranceCompID() {
        return insuranceCompID;
    }

    public void setInsuranceCompID(String insuranceCompID) {
        this.insuranceCompID = insuranceCompID;
    }

    public String getClaimID() {
        return claimID;
    }

    public void setClaimID(String claimID) {
        this.claimID = claimID;
    }

    public String getAgentID() {
        return agentID;
    }

    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    public String getSupervisorID() {
        return supervisorID;
    }

    public void setSupervisorID(String supervisorID) {
        this.supervisorID = supervisorID;
    }

    public String getAuditorID() {
        return auditorID;
    }

    public void setAuditorID(String auditorID) {
        this.auditorID = auditorID;
    }

    public HospitalWorkRequest getHospitalReq() {
        return hospitalReq;
    }

    public void setHospitalReq(HospitalWorkRequest hospitalReq) {
        this.hospitalReq = hospitalReq;
    }

    public ArrayList<Remark> getRemarkList() {
        return remarkList;
    }

    public void setRemarkList(ArrayList<Remark> remarkList) {
        this.remarkList = remarkList;
    }

}
