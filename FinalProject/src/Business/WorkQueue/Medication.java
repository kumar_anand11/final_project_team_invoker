/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author hp
 */
public class Medication {
    private double temperature;
    private double bloodPressure;
    private int pulse;
    private Date appointmentDate ;
    private Boolean labReqrd;
    private Date nextAppointmentDate ;
    private String medicalNotes;
    private String prescribedMedicines1;
    private String prescribedMedicines2;
    private String prescribedMedicines3;
    private String prescribedMedicines4;
    private String prescribedMedicines5;
    private int qty1;
    private int qty2;
    private int qty3;
    private int qty4;
    private int qty5;
    private String labtype;

    public String getPrescribedMedicines4() {
        return prescribedMedicines4;
    }

    public void setPrescribedMedicines4(String prescribedMedicines4) {
        this.prescribedMedicines4 = prescribedMedicines4;
    }

    public String getPrescribedMedicines5() {
        return prescribedMedicines5;
    }

    public void setPrescribedMedicines5(String prescribedMedicines5) {
        this.prescribedMedicines5 = prescribedMedicines5;
    }

    public int getQty4() {
        return qty4;
    }

    public void setQty4(int qty4) {
        this.qty4 = qty4;
    }

    public int getQty5() {
        return qty5;
    }

    public void setQty5(int qty5) {
        this.qty5 = qty5;
    }

    public String getPrescribedMedicines1() {
        return prescribedMedicines1;
    }

    public void setPrescribedMedicines1(String prescribedMedicines1) {
        this.prescribedMedicines1 = prescribedMedicines1;
    }

    public String getPrescribedMedicines2() {
        return prescribedMedicines2;
    }

    public void setPrescribedMedicines2(String prescribedMedicines2) {
        this.prescribedMedicines2 = prescribedMedicines2;
    }

    public String getPrescribedMedicines3() {
        return prescribedMedicines3;
    }

    public void setPrescribedMedicines3(String prescribedMedicines3) {
        this.prescribedMedicines3 = prescribedMedicines3;
    }

    public int getQty1() {
        return qty1;
    }

    public void setQty1(int qty1) {
        this.qty1 = qty1;
    }

    public int getQty2() {
        return qty2;
    }

    public void setQty2(int qty2) {
        this.qty2 = qty2;
    }

    public int getQty3() {
        return qty3;
    }

    public void setQty3(int qty3) {
        this.qty3 = qty3;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(double bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public Date getNextAppointmentDate() {
        return nextAppointmentDate;
    }

    public void setNextAppointmentDate(Date nextAppointmentDate) {
        this.nextAppointmentDate = nextAppointmentDate;
    }

    public String getLabtype() {
        return labtype;
    }

    public void setLabtype(String labtype) {
        this.labtype = labtype;
    }



    public Boolean getLabReqrd() {
        return labReqrd;
    }

    public void setLabReqrd(Boolean labReqrd) {
        this.labReqrd = labReqrd;
    }


    public String getMedicalNotes() {
        return medicalNotes;
    }

    public void setMedicalNotes(String medicalNotes) {
        this.medicalNotes = medicalNotes;
    }
}
