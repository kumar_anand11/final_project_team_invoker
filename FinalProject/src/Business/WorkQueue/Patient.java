/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author Shashank
 */
public class Patient {
    public static int count = 0;
    private String patientID;
    private String firstName;
    private String lastName;
    private String phoneNum;
    private Date dob;
    private int age;
    private String eMail;
    private String height;
    private String weight;
    private String ssn;
    private Address address;

    public String getSymptoms() {
        return symptoms;
    }
    private String symptoms;
    private Policy policy;
    private String natureOfVisit;
    
    //Constructor
    public Patient(){
        count++;
        patientID="P_"+count;
    }
    
    
    
    //Getters & Setters
    public String getPatientID() {    
        return patientID;
    }

    public void setPatientID(String patientID) {    
        this.patientID = patientID;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Patient.count = count;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }
    
    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public String getNatureOfVisit() {
        return natureOfVisit;
    }

    public void setNatureOfVisit(String natureOfVisit) {
        this.natureOfVisit = natureOfVisit;
    }
    
    

  
    //Constructor
    /*public Patient(String firstName, String lastName, String phoneNum,String eMail, Date dob, int age, String height, String weight, String ssn, String symptoms, Policy policy) {
        //set values
        count++;
        patientID="P_"+count;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.eMail=eMail;
        this.dob = dob;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.ssn = ssn;
        this.symptoms = symptoms;
        this.policy=policy;
    }*/
    
    @Override
    public String toString(){
        return " ["+patientID+" "+
                    firstName+" "+
                    lastName+" "+
                    phoneNum+" "+
                    dob+" "+
                    age+" "+
                    eMail+" "+
                    height+" "+
                    weight+" "+
                    ssn+" "+
                    address+" "+
                    symptoms+" "+
                    policy+" "+
                    natureOfVisit+" ]";
    }
    
}
