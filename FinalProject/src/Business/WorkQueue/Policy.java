/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author Shashank
 */
public class Policy {
    private String policyID;
    private Date startDate;
    private Date endDate;
    private String policyGrade;
    private String insuranceComp;
    private double pastClaimedAmnt;
    
    //Constructor

    public Policy(String policyID, Date startDate, Date endDate, String policyGrade, String insuranceComp) {
        this.policyID = policyID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.policyGrade = policyGrade;
        this.insuranceComp = insuranceComp;
    }
    
    
    //Getters & Setters
    public String getPolicyID() {
        return policyID;
    }

    public void setPolicyID(String policyID) {
        this.policyID = policyID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPolicyGrade() {
        return policyGrade;
    }

    public void setPolicyGrade(String policyGrade) {
        this.policyGrade = policyGrade;
    }

    public String getInsuranceComp() {
        return insuranceComp;
    }

    public void setInsuranceComp(String insuranceComp) {
        this.insuranceComp = insuranceComp;
    }
	
	public double getPastClaimedAmnt() {
        return pastClaimedAmnt;
    }

    public void setPastClaimedAmnt(double pastClaimedAmnt) {
        this.pastClaimedAmnt = pastClaimedAmnt;
    }
    
    @Override
    public String toString(){
        return "["+policyID+" "+
                    startDate+" "+
                    endDate+" "+
                    policyGrade+" "+
                    insuranceComp+"] ";
    }
    
}
