/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.mailUtility;

import javax.mail.PasswordAuthentication;

/**
     * * SimpleAuthenticator is used to do simple authentication * when the SMTP
     * server requires it.
     */
    public class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            String username = SendMailUsingAuthentication.SMTP_AUTH_USER;
            String password = SendMailUsingAuthentication.SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }
