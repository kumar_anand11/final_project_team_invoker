/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Agent;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AgentOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.Organization;
import Business.Organization.SupervisorOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.InsuranceWorkRequest;
import Business.WorkQueue.Remark;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Shashank
 */
//Class for Create-Form for Personal-Information application
public class ProcessClaimJPanel extends javax.swing.JPanel {

    //Variable Declaration
    private Logger log=Logger.getLogger(ProcessClaimJPanel.class);
    private InsuranceWorkRequest request;
    //Variable declarations
    private JPanel userProcessContainer;
    private AgentOrganization org;
    private Enterprise enterprise;
    private EcoSystem ecosys;
    UserAccount account;
    private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    /**
     * Creates new form createJPanel
     */
    public ProcessClaimJPanel(JPanel userProcessContainer, AgentOrganization org, Enterprise enterprise, EcoSystem ecosystem, InsuranceWorkRequest iwr, UserAccount ua) {
        log.debug("Entering ProcessClaimJPanel Agent constructor");
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.ecosys=ecosystem;
        this.enterprise=enterprise;
        this.org=org;
        this.request=iwr;
        this.account=ua;
        
        populate();
        log.debug("Exiting ProcessClaimJPanel Agent constructor");
        //Hiding field to store path of uploaded photo
        //this.filePathTextField.setVisible(false);

    }
    
    public String getDoctorName(String docID) {
        String name = null;
        for (Network network : ecosys.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("hospital")) {
                    for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (organization instanceof DoctorOrganization) {
                            for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                if (ua.getUsername().equals(docID)) {
                                    System.out.println("Doctor-Name :" + ua.getEmployee().getLastName()+", "+ua.getEmployee().getFirstName());
                                    name = ua.getEmployee().getLastName()+", "+ua.getEmployee().getFirstName();
                                    break;
                                }
                            }
                        }
                    }

                }
            }
        }
        return name;
    }
    
    public void populate() {
        try{
        log.debug("Entering ProcessClaimJPanel Agent populate");
        txtPolicyID.setText(request.getHospitalReq().getPatient().getPolicy().getPolicyID());
        txtStartDate.setText(sf.format(request.getHospitalReq().getPatient().getPolicy().getStartDate()));
        txtEndDate.setText(sf.format(request.getHospitalReq().getPatient().getPolicy().getEndDate()));
        txtGrade.setText(request.getHospitalReq().getPatient().getPolicy().getPolicyGrade());
        txtPolHoldName.setText((request.getHospitalReq().getPatient().getLastName() + ", " + request.getHospitalReq().getPatient().getFirstName()));

        txtCLaimId.setText(request.getClaimID());
        txtHospitalName.setText(request.getHospitalReq().getHospitalID());
        txtDoctorName.setText(getDoctorName(request.getHospitalReq().getDoctorID()));
        txtAmtClaimed.setText(String.valueOf(request.getClaimAmnt()));
        txtVisitCharges.setText(String.valueOf(request.getHospitalReq().getTotalBill()));
        txtLabCharges.setText(String.valueOf(request.getHospitalReq().getLabFees()));

        //Get Claim-History
        DefaultTableModel dtm = (DefaultTableModel) tblClaimHist.getModel();
        dtm.setRowCount(0);
        double pastClaimAmt=0.00;
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
            for (WorkRequest wr : org.getWorkQueue().getWorkRequestList()) {
                InsuranceWorkRequest iwr = (InsuranceWorkRequest) wr;
                if (iwr.getPolicy().getPolicyID().equals(request.getPolicy().getPolicyID()) 
                        && !iwr.getClaimID().equals(request.getClaimID())) {
                    Object[] row = new Object[6];
                    row[0] = iwr.getClaimID();
                    row[1] = sf.format(iwr.getRequestDate());
                    row[2] = iwr.getHospitalReq().getHospitalID();
                    row[3] = iwr.getStatus();
                    row[4] = iwr.getClaimAmnt();
                    row[5] = iwr.getClaimApprvAmnt();
                    pastClaimAmt+=iwr.getClaimAmnt();
                    dtm.addRow(row);
                }
            }
        }
        request.getPolicy().setPastClaimedAmnt(pastClaimAmt);
        //Communication-History
        dtm = (DefaultTableModel) tblCommHist.getModel();
        dtm.setRowCount(0);
        boolean flagComment = false;
        Remark delRemark = null;
        for (Remark rem : request.getRemarkList()) {
            if (null != rem.getStatus() && rem.getStatus().equalsIgnoreCase("save")) {
                flagComment = true;
                delRemark = rem;
            } else {
                Object[] row = new Object[4];
                row[0] = sf.format(rem.getCreationDate());
                row[1] = rem.getUsername();
                row[2] = rem.getRole();
                row[3] = rem.getComment();
                dtm.addRow(row);
            }
        }
        if (flagComment) {
            txtRemarks.setText(delRemark.getComment());
            request.getRemarkList().remove(delRemark);
        } else {
            txtRemarks.setText("");
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimJPanel Agent populate");
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLbl = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        streetAddrLbl = new javax.swing.JLabel();
        txtCLaimId = new javax.swing.JTextField();
        cityLbl = new javax.swing.JLabel();
        txtHospitalName = new javax.swing.JTextField();
        stateLbl = new javax.swing.JLabel();
        txtDoctorName = new javax.swing.JTextField();
        zipLbl = new javax.swing.JLabel();
        txtAmtClaimed = new javax.swing.JTextField();
        zipLbl1 = new javax.swing.JLabel();
        txtVisitCharges = new javax.swing.JTextField();
        zipLbl2 = new javax.swing.JLabel();
        txtLabCharges = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        svBnkNamLbl = new javax.swing.JLabel();
        txtPolicyID = new javax.swing.JTextField();
        svBnkRoutngNumLbl1 = new javax.swing.JLabel();
        svBnkNamLbl4 = new javax.swing.JLabel();
        txtPolHoldName = new javax.swing.JTextField();
        svBnkAccBalLbl = new javax.swing.JLabel();
        txtGrade = new javax.swing.JTextField();
        svBnkRoutngNumLbl = new javax.swing.JLabel();
        txtStartDate = new javax.swing.JTextField();
        txtEndDate = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblClaimHist = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCommHist = new javax.swing.JTable();
        btnSndToSuper = new javax.swing.JButton();
        streetAddrLbl1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtRemarks = new javax.swing.JTextArea();
        btnSaveRemarks = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 153, 153));

        titleLbl.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        titleLbl.setText("CLAIM-PROCESS FORM");

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Claim Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel2.setMinimumSize(new java.awt.Dimension(50, 50));

        streetAddrLbl.setText("Claim-ID:");

        txtCLaimId.setEditable(false);

        cityLbl.setText("Hospital-Name:");

        txtHospitalName.setEditable(false);

        stateLbl.setText("Doctor-Name:");

        txtDoctorName.setEditable(false);

        zipLbl.setText("Amount Claimed:");

        txtAmtClaimed.setEditable(false);

        zipLbl1.setText("Visit-Charges:");

        txtVisitCharges.setEditable(false);

        zipLbl2.setText("Lab-Charges:");

        txtLabCharges.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(streetAddrLbl)
                    .addComponent(zipLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCLaimId, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                    .addComponent(txtAmtClaimed))
                .addGap(90, 90, 90)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cityLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtVisitCharges, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(txtHospitalName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(zipLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtLabCharges, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(stateLbl)
                        .addGap(35, 35, 35)
                        .addComponent(txtDoctorName, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(streetAddrLbl)
                    .addComponent(txtCLaimId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cityLbl)
                    .addComponent(txtHospitalName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateLbl)
                    .addComponent(txtDoctorName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(zipLbl)
                    .addComponent(txtAmtClaimed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLbl1)
                    .addComponent(txtVisitCharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLbl2)
                    .addComponent(txtLabCharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Policy Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel3.setMinimumSize(new java.awt.Dimension(50, 50));

        svBnkNamLbl.setText("Policy-ID:");

        txtPolicyID.setEditable(false);

        svBnkRoutngNumLbl1.setText("Start-Date:");

        svBnkNamLbl4.setText("Policy-Holder's Name:");

        txtPolHoldName.setEditable(false);

        svBnkAccBalLbl.setText("Policy-Grade:");

        txtGrade.setEditable(false);

        svBnkRoutngNumLbl.setText("End-Date:");

        txtStartDate.setEditable(false);

        txtEndDate.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl)
                    .addComponent(svBnkRoutngNumLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolicyID, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(txtStartDate))
                .addGap(53, 53, 53)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl4)
                    .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolHoldName)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addComponent(svBnkAccBalLbl)
                .addGap(39, 39, 39)
                .addComponent(txtGrade, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(svBnkNamLbl)
                    .addComponent(txtPolicyID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(svBnkNamLbl4)
                    .addComponent(txtPolHoldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(svBnkAccBalLbl)
                    .addComponent(txtGrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(svBnkRoutngNumLbl1)
                    .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(0, 153, 153));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Claim History", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel7.setMinimumSize(new java.awt.Dimension(50, 50));

        tblClaimHist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Claim-ID", "Claim-Date", "Hospital-Name", "Claim-Status", "Claim-Amount", "Approved-Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblClaimHist);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 250, 205));
        jPanel8.setOpaque(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 472, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 250, 205));
        jPanel9.setOpaque(false);

        btnBack.setText("<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addGap(583, 583, 583))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(0, 153, 153));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Communication History", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel10.setMinimumSize(new java.awt.Dimension(50, 50));

        tblCommHist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date", "Name", "Role", "Comments"
            }
        ));
        jScrollPane2.setViewportView(tblCommHist);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnSndToSuper.setText("Send To Supervisor");
        btnSndToSuper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSndToSuperActionPerformed(evt);
            }
        });

        streetAddrLbl1.setText("Remarks*:");

        txtRemarks.setColumns(20);
        txtRemarks.setRows(5);
        jScrollPane3.setViewportView(txtRemarks);

        btnSaveRemarks.setText("Save Remarks");
        btnSaveRemarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveRemarksActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(383, 383, 383))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(668, 668, 668)
                                        .addComponent(btnSaveRemarks))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(streetAddrLbl1)
                                        .addGap(941, 941, 941)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 851, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(btnSndToSuper))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLbl)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(titleLbl)
                        .addGap(40, 40, 40)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetAddrLbl1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSaveRemarks)
                    .addComponent(btnSndToSuper))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    //Method to set the values from screen-fields to respective object-attributes
    private void btnSndToSuperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSndToSuperActionPerformed
        // TODO add your handling code here:
        //Validations
        try{
            
        log.debug("Entering ProcessClaimJPanel Agent btnSndToSuperActionPerformed");
        if (txtRemarks.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Remarks for the Claim cannot be blank!");
            return;
        } else {
            //Show error message or set values in objects
            //Binding screen values to object attributes
            Remark rem=new Remark();
            rem.setRole("Agent");
            rem.setUsername(account.getUsername());
            rem.setComment(txtRemarks.getText());
            rem.setStatus("SNDSUP");
            request.getRemarkList().add(rem);
            
            
            
            request.setStatus("Assigned to Supervisor");
            try{
            //Add this Work-Request to queue
            Organization supOrg = null;
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                if (organization instanceof SupervisorOrganization) {
                    supOrg = organization;
                    break;
                }
            }
            if (supOrg != null) {
                supOrg.getWorkQueue().getWorkRequestList().add(request);
                int minCOunt=0;
                String userName=supOrg.getUserAccountDirectory().getUserAccountList().get(0).getUsername();
                for (UserAccount ua : supOrg.getUserAccountDirectory().getUserAccountList()) {
                    if(minCOunt>ua.getWorkQueue().getWorkRequestList().size()){
                        userName=ua.getUsername();
                    }
                }
                
                for (UserAccount ua : supOrg.getUserAccountDirectory().getUserAccountList()) {
                    if(userName.equals(ua.getUsername())){
                        ua.getWorkQueue().getWorkRequestList().add(request);
                        request.setSupervisorID(userName);
                    }
                }
            }

            account.getWorkQueue().getWorkRequestList().remove(request);
            org.getWorkQueue().getWorkRequestList().remove(request);
            
            //Prompt user that details were saved successfully
            JOptionPane.showMessageDialog(this, "Request sent to Supervisor successfully");
            btnSaveRemarks.setEnabled(false);
            btnSndToSuper.setEnabled(false);
            
            }catch(Exception e){
               JOptionPane.showMessageDialog(this, "System emcountered an error while assigning request to Supervisor.","Inane error",JOptionPane.ERROR_MESSAGE); 
            }

        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimJPanel Agent btnSndToSuperActionPerformed");
    }//GEN-LAST:event_btnSndToSuperActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSaveRemarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveRemarksActionPerformed
        
        try{
            log.debug("Entering ProcessClaimJPanel Agent btnResetFormActionPerformed");
            if (txtRemarks.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Remarks for the Claim cannot be blank!");
            return;
        } else {
            //Show error message or set values in objects
            //Binding screen values to object attributes
            Remark rem = new Remark();
            rem.setRole("Agent");
            rem.setUsername(account.getUsername());
            rem.setComment(txtRemarks.getText());
            rem.setStatus("SAVE");
            request.getRemarkList().add(rem);
            JOptionPane.showMessageDialog(this, "Remarks saved successfully!");
            btnSaveRemarks.setEnabled(false);
            btnSndToSuper.setEnabled(false);
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimJPanel Agent btnResetFormActionPerformed");
    }//GEN-LAST:event_btnSaveRemarksActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSaveRemarks;
    private javax.swing.JButton btnSndToSuper;
    private javax.swing.JLabel cityLbl;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel stateLbl;
    private javax.swing.JLabel streetAddrLbl;
    private javax.swing.JLabel streetAddrLbl1;
    private javax.swing.JLabel svBnkAccBalLbl;
    private javax.swing.JLabel svBnkNamLbl;
    private javax.swing.JLabel svBnkNamLbl4;
    private javax.swing.JLabel svBnkRoutngNumLbl;
    private javax.swing.JLabel svBnkRoutngNumLbl1;
    private javax.swing.JTable tblClaimHist;
    private javax.swing.JTable tblCommHist;
    private javax.swing.JLabel titleLbl;
    private javax.swing.JTextField txtAmtClaimed;
    private javax.swing.JTextField txtCLaimId;
    private javax.swing.JTextField txtDoctorName;
    private javax.swing.JTextField txtEndDate;
    private javax.swing.JTextField txtGrade;
    private javax.swing.JTextField txtHospitalName;
    private javax.swing.JTextField txtLabCharges;
    private javax.swing.JTextField txtPolHoldName;
    private javax.swing.JTextField txtPolicyID;
    private javax.swing.JTextArea txtRemarks;
    private javax.swing.JTextField txtStartDate;
    private javax.swing.JTextField txtVisitCharges;
    private javax.swing.JLabel zipLbl;
    private javax.swing.JLabel zipLbl1;
    private javax.swing.JLabel zipLbl2;
    // End of variables declaration//GEN-END:variables
}
