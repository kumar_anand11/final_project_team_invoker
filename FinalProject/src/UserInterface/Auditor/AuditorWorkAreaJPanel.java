/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Auditor;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.*;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HospitalWorkRequest;
import Business.WorkQueue.InsuranceWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author hp
 */
public class AuditorWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AuditorWorkAreaJPanel
     */
    private Logger log=Logger.getLogger(AuditorWorkAreaJPanel.class);
    private JPanel userProcessContainer;
    private UserAccount userAccount; 
    private AuditorOrganization organization; 
    private Enterprise enterprise; 
    private EcoSystem ecosystem;
    private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    
    public AuditorWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, AuditorOrganization organization, Enterprise enterprise, EcoSystem ecosystem) {
        log.debug("Entering AuditorWorkAreaJPanel Auditor constrcutor");
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.userAccount=account;
        this.organization=organization;
        this.enterprise=enterprise;
        this.ecosystem=ecosystem;
        docLabel.setText(userAccount.getUsername());
        populateTable();
        populateDropDowns();
        log.debug("Exiting AuditorWorkAreaJPanel Auditor constrcutor");
        
    }
    public void populateDropDowns(){
        insuranceStatusdd.removeAllItems();
        insuranceStatusdd.addItem("All");
        insuranceStatusdd.addItem("New");
        insuranceStatusdd.addItem("Assigned to Agent");
        insuranceStatusdd.addItem("Assigned to Supervisor");
        insuranceStatusdd.addItem("Assigned to Auditor");
        insuranceStatusdd.addItem("Claim Rejected");
        insuranceStatusdd.addItem("Claim Settled");
    }
    
    public void populateTable()
    {
        try{
            
        log.debug("Entering AuditorWorkAreaJPanel Auditor populateTable");
        DefaultTableModel dtm = (DefaultTableModel)insurancejTable.getModel();
        dtm.setRowCount(0);
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList())
                    {
                        for (WorkRequest wr : organization.getWorkQueue().getWorkRequestList())
                        {
                          InsuranceWorkRequest request = (InsuranceWorkRequest) wr;                         
                           if (null == request.getAuditorID()) {
                             request.setAuditorID("");
                           }
                          if (request.getAuditorID().contentEquals(userAccount.getUsername())){  
                            Object[] row = new Object[9];                           
                            row[0] = request.getClaimID();
                            row[1] = request.getPolicy().getPolicyID();
                            row[2] = request.getHospitalReq().getHospitalID();
                            row[3] = request.getClaimAmnt();
                            row[4] = request.getClaimApprvAmnt();
                            row[5] = sf.format(request.getRequestDate());
                            if (request.getStatus().contentEquals("Claim Rejected") || 
                                request.getStatus().contentEquals("Claim Settled"))
                            {
                              row[6] = sf.format(request.getResolveDate());  
                            }else{
                                row[6] = "-";
                            }
                            row[7] = request.getStatus();

                            long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
                            if (diff > 1) {
                                row[8] = diff ;
                            }
                            if (diff <= 1){
                                row[8] = 0;
                            }
                            dtm.addRow(row);
                          }
                        }
            }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting AuditorWorkAreaJPanel Auditor populateTable");
        }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        claimjButton = new javax.swing.JButton();
        msgJLabel = new javax.swing.JLabel();
        docLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnRefresh = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        insurancejTable = new javax.swing.JTable();
        insuranceStatusdd = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        progressbtn = new javax.swing.JButton();
        prgrssBar = new javax.swing.JProgressBar();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        pieChartBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 153, 153));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Auditor Work Area");
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Welcome !!");

        claimjButton.setText("Process Claim");
        claimjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                claimjButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        jLabel6.setText("Status :");

        insurancejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Claim Id", "Policy Id", "Hospital Id", "Claim Amount", "Approved Amount", "Claim Date", "Closure Date", "Claim Status", "Ageing"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(insurancejTable);

        insuranceStatusdd.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        insuranceStatusdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insuranceStatusddActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("OR");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Claim Id:");

        progressbtn.setText("Progress");
        progressbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                progressbtnActionPerformed(evt);
            }
        });

        jLabel8.setText("0%");

        jLabel9.setText("25%");

        jLabel10.setText("50%");

        jLabel11.setText("75%");

        jLabel12.setText("100%");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(insuranceStatusdd, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(jLabel5)
                        .addGap(47, 47, 47)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefresh))
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(progressbtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                                .addComponent(jLabel9)
                                .addGap(199, 199, 199)
                                .addComponent(jLabel10)
                                .addGap(213, 213, 213)
                                .addComponent(jLabel11)
                                .addGap(188, 188, 188)
                                .addComponent(jLabel12))
                            .addComponent(prgrssBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(insuranceStatusdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(progressbtn)
                    .addComponent(prgrssBar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pieChartBtn.setText("Claim Requests Distribution");
        pieChartBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pieChartBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(msgJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(docLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel7)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(pieChartBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(claimjButton)))
                .addContainerGap(84, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel7)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(msgJLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(docLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(claimjButton)
                    .addComponent(pieChartBtn))
                .addGap(124, 124, 124))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void insuranceStatusddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insuranceStatusddActionPerformed
        // TODO add your handling code here:
        try{
        log.debug("Entering AuditorWorkAreaJPanel Auditor insuranceStatusddActionPerformed");
            txtSearch.setText("");
            prgrssBar.setValue(0);
        if (null!=insuranceStatusdd.getSelectedItem()) {
            String status = insuranceStatusdd.getSelectedItem().toString();
            DefaultTableModel dtm = (DefaultTableModel) insurancejTable.getModel();
            boolean flgZero = true;
            for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                for (WorkRequest wr : org.getWorkQueue().getWorkRequestList()) {
                    InsuranceWorkRequest request = (InsuranceWorkRequest) wr;
                    if (request.getAuditorID().contentEquals(userAccount.getUsername())){ 
                        if (request.getStatus().equals(status)) {
                            if (flgZero) {
                                dtm.setRowCount(0);
                                flgZero = false;
                            }
                            Object[] row = new Object[9];                           
                            row[0] = request.getClaimID();
                            row[1] = request.getPolicy().getPolicyID();
                            row[2] = request.getHospitalReq().getHospitalID();
                            row[3] = request.getClaimAmnt();
                            row[4] = request.getClaimApprvAmnt();
                            row[5] = sf.format(request.getRequestDate());
                            if (request.getStatus().contentEquals("Claim Rejected") || 
                                request.getStatus().contentEquals("Claim Settled"))
                            {
                              row[6] = sf.format(request.getResolveDate());  
                            }else{
                                row[6] = "-";
                            }
                            row[7] = request.getStatus();
                            long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
                            if (diff > 1) {
                                row[8] = diff ;
                            }
                            if (diff <= 1){
                                row[8] = 0;
                            }
                            dtm.addRow(row);
                        } else if (status.equalsIgnoreCase("all")) {
                            if (flgZero) {
                                dtm.setRowCount(0);
                                flgZero = false;
                            }
                            Object[] row = new Object[9];                           
                            row[0] = request.getClaimID();
                            row[1] = request.getPolicy().getPolicyID();
                            row[2] = request.getHospitalReq().getHospitalID();
                            row[3] = request.getClaimAmnt();
                            row[4] = request.getClaimApprvAmnt();
                            row[5] = sf.format(request.getRequestDate());
                            if (request.getStatus().contentEquals("Claim Rejected") || 
                                request.getStatus().contentEquals("Claim Settled"))
                            {
                              row[6] = sf.format(request.getResolveDate());  
                            }else{
                                row[6] = "-";
                            }
                            row[7] = request.getStatus();
                            long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
                            if (diff > 1) {
                                row[8] = diff;
                            }
                            if (diff <= 1){
                                row[8] = 0;
                            }
                            dtm.addRow(row);
                        }
                    }
                }
            }
            if (flgZero) {
                dtm.setRowCount(0);
                JOptionPane.showMessageDialog(this, "No Claims found!");
            }
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting AuditorWorkAreaJPanel Auditor insuranceStatusddActionPerformed");
    }//GEN-LAST:event_insuranceStatusddActionPerformed

    private void claimjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_claimjButtonActionPerformed
        // TODO add your handling code here:
        try {
            log.debug("Entering AuditorWorkAreaJPanel Auditor claimjButtonActionPerformed");
            int selectedRow = insurancejTable.getSelectedRow();
            if (selectedRow < 0) {
                JOptionPane.showMessageDialog(null, "Please select a row");
                return;
            } else {

                String id = (String) insurancejTable.getValueAt(selectedRow, 0);
                InsuranceWorkRequest request = null;
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (WorkRequest wr : organization.getWorkQueue().getWorkRequestList()) {
                        InsuranceWorkRequest hwr = (InsuranceWorkRequest) wr;
                        if (hwr.getClaimID().equals(id)) {
                            request = hwr;
                            break;
                        }
                    }
                }

                if (!request.getAuditorID().contentEquals(userAccount.getUsername())) {
                    JOptionPane.showMessageDialog(null, "You don't have the ownership of this claim!!!!");
                    return;
                }

                if (request.getStatus().contentEquals("Assigned to Auditor")) {
                    CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                    userProcessContainer.add("ProcessClaimAuditorJPanel", new ProcessClaimAuditorJPanel(userProcessContainer, userAccount, (AuditorOrganization) organization, enterprise, ecosystem, request));
                    layout.next(userProcessContainer);
                } else {
                    JOptionPane.showMessageDialog(null, "The claim either already processed or yet to ariive to your queue");
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());

        }
        log.debug("Exiting AuditorWorkAreaJPanel Auditor claimjButtonActionPerformed");

    }//GEN-LAST:event_claimjButtonActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        prgrssBar.setValue(0);
        populateTable();
        populateDropDowns();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        try{
            
        log.debug("Entering AuditorWorkAreaJPanel Auditor btnSearchActionPerformed");
        String search="";
        prgrssBar.setValue(0);
        if(txtSearch.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Please enter the Claim-Id to be searched!");
            return;
        }else{
            search=txtSearch.getText();
            txtSearch.setBorder(null);
            txtSearch.updateUI();
            insuranceStatusdd.setSelectedIndex(0);
        }

        DefaultTableModel dtm = (DefaultTableModel) insurancejTable.getModel();
        boolean flgZero=true;
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
            for (WorkRequest wr : org.getWorkQueue().getWorkRequestList()) {
                InsuranceWorkRequest request = (InsuranceWorkRequest) wr;
            //   if (request.getAuditorID().contentEquals(userAccount.getUsername())){
                if (request.getClaimID().equals(search)) {
                    if(flgZero){
                        dtm.setRowCount(0);
                        flgZero=false;
                    }
                        Object[] row = new Object[9];                           
                        row[0] = request.getClaimID();
                        row[1] = request.getPolicy().getPolicyID();
                        row[2] = request.getHospitalReq().getHospitalID();
                        row[3] = request.getClaimAmnt();
                        row[4] = request.getClaimApprvAmnt();
                        row[5] = sf.format(request.getRequestDate());
                            if (request.getStatus().contentEquals("Claim Rejected") || 
                                request.getStatus().contentEquals("Claim Settled"))
                            {
                              row[6] = sf.format(request.getResolveDate());  
                            }else{
                                row[6] ="-" ;
                            }
                        row[7] = request.getStatus();
                            long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
                            if (diff > 1) {
                                row[8] = diff ;
                            }
                            if (diff <= 1){
                                row[8] = 0;
                            }
                        dtm.addRow(row);
                }
           // }
        }
        }
        if(flgZero){
            dtm.setRowCount(0);
            JOptionPane.showMessageDialog(this, "No claim with ID: "+search+" exists!!");
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting AuditorWorkAreaJPanel Auditor btnSearchActionPerformed");

    }//GEN-LAST:event_btnSearchActionPerformed

    private void pieChartBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pieChartBtnActionPerformed
        // TODO add your handling code here:
        int cnt = 0 ;
        int otherscnt =0 ;
        int newcnt = 0 ;
        int setlcnt = 0 ;
        int rejcnt = 0 ;
        int suprcnt = 0;
        int agcnt = 0 ;
        int aucnt= 0 ;
        DefaultPieDataset pieDataset = new DefaultPieDataset() ;
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList())
        {
            for (WorkRequest wr : organization.getWorkQueue().getWorkRequestList())
            {
                InsuranceWorkRequest request = (InsuranceWorkRequest) wr;
                if (request.getStatus().contentEquals("New")){
                    newcnt++;
                }
                if (request.getAuditorID().contentEquals(userAccount.getUsername()))

                {
                    cnt++;
                    if (request.getStatus().contentEquals("Assigned to Agent")){
                        agcnt++;
                    }
                    if (request.getStatus().contentEquals("Assigned to Supervisor")){
                        suprcnt++;
                    }
                    if (request.getStatus().contentEquals("Assigned to Auditor")){
                        aucnt++;
                    }
                    if (request.getStatus().contentEquals("Claim Rejected")){
                        rejcnt++;
                    }
                    if (request.getStatus().contentEquals("Claim Settled")){
                        setlcnt++;
                    }
                }
                else{
                    otherscnt++;
                }
            }
        }
        pieDataset.setValue("Pending with New Status",new  Integer(newcnt));
        pieDataset.setValue("Pending with You",new  Integer(aucnt));
        pieDataset.setValue("Pending with Agent",new  Integer(agcnt));
        pieDataset.setValue("Pending with Supervisor",new  Integer(suprcnt));
        pieDataset.setValue("Claim Rejected",new  Integer(rejcnt));
        pieDataset.setValue("Claim Settled",new  Integer(setlcnt));
        JFreeChart chart = ChartFactory.createPieChart3D(userAccount.getUsername()+" claim requests distribution", pieDataset,true,true,true);
        PiePlot3D p = (PiePlot3D)chart.getPlot();
        ChartFrame  frame = new ChartFrame("Claim requests distribution",chart);
        frame.setVisible(true);
        frame.setSize(550,600);

        DefaultPieDataset pieDataset1 = new DefaultPieDataset() ;
        pieDataset1.setValue("New requests",new  Integer(newcnt));
        pieDataset1.setValue("Request with You",new  Integer(aucnt));
        pieDataset1.setValue("Request with Other Auditors",new  Integer(otherscnt));
        JFreeChart chart1 = ChartFactory.createPieChart3D("Total requests distribution", pieDataset1,true,true,true);
        PiePlot3D p1 = (PiePlot3D)chart.getPlot();
        ChartFrame  frame1 = new ChartFrame("Total requests distribution",chart1);
        frame1.setVisible(true);
        frame1.setSize(550,600);
    }//GEN-LAST:event_pieChartBtnActionPerformed

    private void progressbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_progressbtnActionPerformed
        // TODO add your handling code here:

        int selectedRow = insurancejTable.getSelectedRow();
        if (selectedRow < 0){
            prgrssBar.setValue(0);
            JOptionPane.showMessageDialog(null,"You need to select the row to see its progress");
            return;
        }
        else {

            String id = (String)insurancejTable.getValueAt(selectedRow, 0);
            InsuranceWorkRequest request=null;
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList())
            {
                for(WorkRequest wr: organization.getWorkQueue().getWorkRequestList()){
                    InsuranceWorkRequest hwr= (InsuranceWorkRequest)wr;
                    if(hwr.getClaimID().equals(id)){
                        request=hwr;
                        break;
                    }
                }

            }
            if (request.getStatus().contentEquals("New"))
            {
                prgrssBar.setValue(10);
            }
            if (request.getStatus().contentEquals("Assigned to Supervisor"))
            {
                prgrssBar.setValue(60);
            }
            if (request.getStatus().contentEquals("Assigned to Agent"))
            {
                prgrssBar.setValue(40);
            }
            if (request.getStatus().contentEquals("Assigned to Auditor"))
            {
                prgrssBar.setValue(80);
            }
            if (request.getStatus().contentEquals("Claim Rejected"))
            {
                prgrssBar.setValue(100);
            }
            if (request.getStatus().contentEquals("Claim Settled"))
            {
                prgrssBar.setValue(100);
            }
        }
    }//GEN-LAST:event_progressbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton claimjButton;
    private javax.swing.JLabel docLabel;
    private javax.swing.JComboBox insuranceStatusdd;
    private javax.swing.JTable insurancejTable;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel msgJLabel;
    private javax.swing.JButton pieChartBtn;
    private javax.swing.JProgressBar prgrssBar;
    private javax.swing.JButton progressbtn;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
