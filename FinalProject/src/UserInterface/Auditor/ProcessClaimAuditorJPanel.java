/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Auditor;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AuditorOrganization;
import Business.Organization.DoctorAssistantOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HospitalWorkRequest;
import Business.WorkQueue.InsuranceWorkRequest;
import Business.WorkQueue.Remark;
import Business.mailUtility.SendMailUsingAuthentication;
import java.awt.CardLayout;
import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import javax.mail.MessagingException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
/**
 *
 * @author hp
 */
public class ProcessClaimAuditorJPanel extends javax.swing.JPanel {
private JPanel userProcessContainer;
private AuditorOrganization organization;
private Enterprise enterprise;
private EcoSystem ecosystem;
private UserAccount userAccount;
private InsuranceWorkRequest request;
private double claimedPastAmnt;
private Logger log=Logger.getLogger(ProcessClaimAuditorJPanel.class);
private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");

    /**
     * Creates new form ProcessClaimJPanel
     */
    public ProcessClaimAuditorJPanel(JPanel userProcessContainer, UserAccount account, AuditorOrganization organization, Enterprise enterprise,EcoSystem ecosystem, InsuranceWorkRequest request) {
        log.debug("Entering ProcessClaimAuditorJPanel Auditor constrcutor");
        initComponents();        
        this.userProcessContainer=userProcessContainer;
        this.ecosystem=ecosystem;
        this.enterprise=enterprise;
        this.organization=organization;
        this.userAccount=account;
        this.request=request;
        claimIDTextField.setText(request.getClaimID());
        hosptalIDTextField.setText(request.getHospitalReq().getHospitalID());
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); 
        claiDateTextField.setText(dateFormat.format(request.getRequestDate()));        
        claimAmntTextField.setText(String.valueOf(request.getClaimAmnt()));        
        claimPastAmntTextField.setText(String.valueOf(request.getPolicy().getPastClaimedAmnt()));
        claimSatusTextField.setText(request.getStatus());
        policyIdTextField.setText(request.getPolicy().getPolicyID());
        policyStDtextField.setText(dateFormat.format(request.getPolicy().getStartDate()));
        policyEndDtextField.setText(dateFormat.format(request.getPolicy().getEndDate()));
        policyGradeTextField.setText(request.getPolicy().getPolicyGrade());                
        agentIDTextField.setText(request.getAgentID()); 
        superIdTextField.setText(request.getSupervisorID());
        claimedPastAmnt=request.getPolicy().getPastClaimedAmnt();
        long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
        if (diff > 1) 
        {
          agingTextField.setText(diff + "Days")   ;
        }
        if (diff <= 1)
        {
         agingTextField.setText("0 Days");
         }
        Date policyDate = request.getPolicy().getEndDate();
        Date currDate = new Date();
        if (currDate.after(policyDate)){
            policyStatusTextField.setText("Expired");
        }
        else
        {
            policyStatusTextField.setText("Active");
        }            

 //////////////Comment History
        DefaultTableModel dtm = (DefaultTableModel) tblCommHist.getModel(); 
        dtm = (DefaultTableModel) tblCommHist.getModel();
        dtm.setRowCount(0);
        boolean flagComment=false;
        Remark delRemark=null;
        for (Remark rem : request.getRemarkList()) {
            if (null != rem.getStatus() && rem.getStatus().equalsIgnoreCase("save")) {
                flagComment = true;
                delRemark = rem;
            } else {
                Object[] row = new Object[4];
                row[0] = sf.format(rem.getCreationDate());
                row[1] = rem.getUsername();
                row[2] = rem.getRole();
                row[3] = rem.getComment();
                System.out.println(rem.getStatus());
                dtm.addRow(row);
            }
        }
        
        if(flagComment){
           auditorRemarksTextField.setText(delRemark.getComment());
           request.getRemarkList().remove(delRemark);
        }else{
            auditorRemarksTextField.setText("");
        }
        log.debug("Exiting ProcessClaimAuditorJPanel Auditor constrcutor");
    }
    
	 public ArrayList<String> getNameAndMail(String ID, String role) {
        ArrayList<String> result=new ArrayList();
        String name = null;
        String mailID = null;
        System.out.println(ID);
        for (Network network : ecosystem.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("hospital")) {
                    if (request.getHospitalReq().getHospitalID().equals(ent.getName())) {
                        for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                            if (role.equals("DA")) {
                                    if (organization instanceof DoctorAssistantOrganization) {
                                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                        System.out.println(ua.getEmployee().getFirstName() + " " + ua.getEmployee().getLastName());
                                        if (ua.getUsername().equals(ID)) {
                                            name = ua.getEmployee().getFirstName() + " " + ua.getEmployee().getLastName();
                                            mailID= ua.getEmployee().getEmail();
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if (organization instanceof DoctorOrganization) {
                                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                        if (ua.getUsername().equals(ID)) {
                                            name = ua.getEmployee().getFirstName() + " " + ua.getEmployee().getLastName();
                                            mailID= ua.getEmployee().getEmail();
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        result.add(name);
        result.add(mailID);
        //System.out.println("mail   Name"+name+mailID);
        return result;
    }
    
    
    public void sendMail(InsuranceWorkRequest iwr, String auditorRemarks) {
        HospitalWorkRequest hwr=iwr.getHospitalReq();
        String emailMsgTxt = "";
        String emailSubjectTxt = "Regd medical-claim raised to "+iwr.getInsuranceCompID()+" : Claim with ID:"+iwr.getClaimID()+" Processed!";
        String emailFromAddress = SendMailUsingAuthentication.SMTP_AUTH_USER;
        String[] emailList = new String[1];
        
        SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();
        try {
            //Patient Mail
            emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Mr/s. "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br>  Thanks for filing a claim with "+iwr.getInsuranceCompID()+". Please find below the details of your claim:<br> <br><br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> "
                    + "Date-Raised: "+sf.format(iwr.getRequestDate())+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                    + "Doctor's Name: "+getNameAndMail(hwr.getDoctorID(),"Doc").get(0)+"<br> Total-Amount: $ "+iwr.getClaimAmnt()+"<br> </i></b> <br><br> "
                    + "<b><i> Policy-ID: "+iwr.getPolicy().getPolicyID()+"<br> Claim-Approved-Amount: $ "+iwr.getClaimApprvAmnt()+" <br> "
                    + "Status: "+iwr.getStatus()+" <br> Remarks: "+auditorRemarks+" </i></b> "
                    + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+iwr.getInsuranceCompID()+" Team<br> <HR> </BODY> </HTML>";
            emailList[0]=hwr.getPatient().geteMail();
            smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);
            
            //Doctor Mail
            emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Dr. "+getNameAndMail(hwr.getDoctorID(),"Doc").get(0)+",<br>  The following claim raised with "+iwr.getInsuranceCompID()+" has been processed successfully. Please find below the details:<br> <br><br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> "
                    + "Date-Raised: "+sf.format(iwr.getRequestDate())+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                    + "Doctor's Name: "+getNameAndMail(hwr.getDoctorID(),"Doc").get(0)+"<br> Total-Amount: $ "+iwr.getClaimAmnt()+"<br> </i></b> <br><br> "
                    + "<b><i> Policy-ID: "+iwr.getPolicy().getPolicyID()+"<br> Claim-Approved-Amount: $ "+iwr.getClaimApprvAmnt()+" <br> "
                    + "Status: "+iwr.getStatus()+" <br> Remarks: "+auditorRemarks+" </i></b> "
                    + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+iwr.getInsuranceCompID()+" Team<br> <HR> </BODY> </HTML>";
            
            emailList[0]=getNameAndMail(hwr.getDoctorID(),"Doc").get(1);
            smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);
            
            //Doctor Mail
            emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Mr/s. "+getNameAndMail(hwr.getDoctorAssistantID(),"DA").get(0)+",<br>  The following claim filed by you with "+iwr.getInsuranceCompID()+" has been processed successfully. Please find below the details:<br> <br><br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> "
                    + "Date-Raised: "+sf.format(iwr.getRequestDate())+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                    + "Doctor's Name: "+getNameAndMail(hwr.getDoctorID(),"Doc").get(0)+"<br> Total-Amount: $ "+iwr.getClaimAmnt()+"<br> </i></b> <br><br> "
                    + "<b><i> Policy-ID: "+iwr.getPolicy().getPolicyID()+"<br> Claim-Approved-Amount: $ "+iwr.getClaimApprvAmnt()+" <br> "
                    + "Status: "+iwr.getStatus()+" <br> Remarks: "+auditorRemarks+" </i></b> "
                    + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+iwr.getInsuranceCompID()+" Team<br> <HR> </BODY> </HTML>";
            
            emailList[0]=getNameAndMail(hwr.getDoctorAssistantID(),"DA").get(1);
            smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);
            
            
            
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
        //System.out.println("Sucessfully Sent mail to All Users");
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        msgjLabel = new javax.swing.JLabel();
        rejectBtn = new javax.swing.JButton();
        apprvBtn = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        policyIdTextField = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        policyStDtextField = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        policyEndDtextField = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        policyGradeTextField = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        policyStatusTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        imagejLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        superIdTextField = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        claimAmntTextField = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        claimSatusTextField = new javax.swing.JTextField();
        claimIDTextField = new javax.swing.JTextField();
        claimPastAmntTextField = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        claiDateTextField = new javax.swing.JTextField();
        hosptalIDTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        agingTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        agentIDTextField = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        patientjRadioButton = new javax.swing.JRadioButton();
        aprrvAmntTextField = new javax.swing.JTextField();
        hospitaljRadioButton = new javax.swing.JRadioButton();
        jLabel27 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        auditorRemarksTextField = new javax.swing.JTextArea();
        msgJlabel = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblCommHist = new javax.swing.JTable();
        saveBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 153, 153));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Claim Approval");
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        rejectBtn.setText("Reject");
        rejectBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rejectBtnActionPerformed(evt);
            }
        });

        apprvBtn.setText("Approve");
        apprvBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                apprvBtnActionPerformed(evt);
            }
        });

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Policy Details:");
        jLabel19.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Policy ID:");
        jLabel22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyIdTextField.setEditable(false);

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("Policy Start Date:");
        jLabel24.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyStDtextField.setEditable(false);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("Policy End Date:");
        jLabel23.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyEndDtextField.setEditable(false);

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Policy Grade:");
        jLabel25.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyGradeTextField.setEditable(false);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("Policy Satus:");
        jLabel26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyStatusTextField.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel19)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(policyStatusTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(policyGradeTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(policyEndDtextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23)
                            .addComponent(jLabel25)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(policyStDtextField)
                            .addComponent(policyIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(91, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(policyIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(policyStDtextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(policyEndDtextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(policyGradeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(policyStatusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        imagejLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/Auditor/Policy Grades.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagejLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Claim Details:");
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        superIdTextField.setEditable(false);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Pending Since:");
        jLabel18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        claimAmntTextField.setEditable(false);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Pass By Supervisor:");
        jLabel17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        claimSatusTextField.setEditable(false);

        claimIDTextField.setEditable(false);

        claimPastAmntTextField.setEditable(false);

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("Past Claimed Amount:");
        jLabel28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Claim Amount:");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Hospital ID:");
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Claim Status:");
        jLabel14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Claim ID:");
        jLabel9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        claiDateTextField.setEditable(false);

        hosptalIDTextField.setEditable(false);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Pass By Agent:");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        agingTextField.setEditable(false);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Claim Date:");
        jLabel13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        agentIDTextField.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(32, 32, 32)
                                .addComponent(agentIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(claimAmntTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(claimIDTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(agingTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel14)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel28)
                                    .addComponent(jLabel17))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(hosptalIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(70, 70, 70))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(claiDateTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                                .addComponent(claimSatusTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(superIdTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(claimPastAmntTextField, javax.swing.GroupLayout.Alignment.LEADING))))
                    .addComponent(jLabel8))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(13, 13, 13)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(claimIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(hosptalIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(claimAmntTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(claiDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(agingTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(claimSatusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(agentIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(superIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(claimPastAmntTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(0, 153, 153));

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Auditor Remarks:");
        jLabel21.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        patientjRadioButton.setText("Patient");
        patientjRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientjRadioButtonActionPerformed(evt);
            }
        });

        aprrvAmntTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                aprrvAmntTextFieldFocusLost(evt);
            }
        });

        hospitaljRadioButton.setText("Hospital");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Approved Amount:");
        jLabel27.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Claim confirmed with:");
        jLabel20.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        auditorRemarksTextField.setColumns(20);
        auditorRemarksTextField.setRows(5);
        jScrollPane1.setViewportView(auditorRemarksTextField);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(25, 25, 25)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(aprrvAmntTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(msgJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 525, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel20)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(patientjRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(hospitaljRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(patientjRadioButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(hospitaljRadioButton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(aprrvAmntTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel27))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(msgJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(0, 153, 153));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Communication History", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel10.setMinimumSize(new java.awt.Dimension(50, 50));

        tblCommHist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date", "Name", "Role", "Comments"
            }
        ));
        jScrollPane3.setViewportView(tblCommHist);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        saveBtn.setText("Save Remarks");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 551, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(msgjLabel)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnBack)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(saveBtn)
                            .addGap(18, 18, 18)
                            .addComponent(apprvBtn)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(rejectBtn))))
                .addGap(116, 116, 116))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(msgjLabel)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apprvBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rejectBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:

      this.userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) this.userProcessContainer.getLayout();
        layout.next(this.userProcessContainer);
      
    }//GEN-LAST:event_btnBackActionPerformed

    private void patientjRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientjRadioButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientjRadioButtonActionPerformed

    private void apprvBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_apprvBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            
        log.debug("Entering ProcessClaimAuditorJPanel Auditor apprvBtnActionPerformed");
        if (policyStatusTextField.getText().contentEquals("Expired")){
            JOptionPane.showMessageDialog(null,"Policy is expired, please add this as remarks and reject the claim");  
            return;            
        }
        if (!patientjRadioButton.isSelected() || !hospitaljRadioButton.isSelected()){
            JOptionPane.showMessageDialog(null,"Please confirm with both Patient and Hospital");  
            return;
        }
        
        if (auditorRemarksTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Auditor Remarks for the Claim cannot be blank!");
            return;
        }
        claimedPastAmnt = request.getPolicy().getPastClaimedAmnt();
        request.getPolicy().setPastClaimedAmnt(Double.parseDouble(aprrvAmntTextField.getText()) + claimedPastAmnt);
        
        Remark rem=new Remark();
        rem.setRole("Auditor");
        rem.setUsername(userAccount.getUsername());
        rem.setComment(auditorRemarksTextField.getText());
        rem.setCreationDate(new Date());
        request.getRemarkList().add(rem);
        request.setStatus("Claim Settled");        
        request.setResolveDate(new Date());
        userAccount.getWorkQueue().getWorkRequestList().remove(request);
       // organization.getWorkQueue().getWorkRequestList().remove(request);        
        JOptionPane.showMessageDialog(null,"Claim approved successfully...and will be settled in few hours");
        apprvBtn.setEnabled(false);
            rejectBtn.setEnabled(false);
            saveBtn.setEnabled(false);
            sendMail(request,auditorRemarksTextField.getText());
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimAuditorJPanel Auditor apprvBtnActionPerformed");
    }//GEN-LAST:event_apprvBtnActionPerformed

    private void aprrvAmntTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_aprrvAmntTextFieldFocusLost
        // TODO add your handling code here:
        try{
            
        log.debug("Entering ProcessClaimAuditorJPanel Auditor aprrvAmntTextFieldFocusLost");
        apprvBtn.setEnabled(true);
        aprrvAmntTextField.setBackground(Color.white);
        msgJlabel.setText("");
        double apprvAmnt = Double.parseDouble(aprrvAmntTextField.getText());
       switch(request.getPolicy().getPolicyGrade()){
           case "Grade-A":
               if (claimedPastAmnt + apprvAmnt > 1000000.00) 
                 {
                 aprrvAmntTextField.setBackground(Color.red);
                 msgJlabel.setText("***Approved amount and past claim amount is exceeding the policy grade limit");
                 apprvBtn.setEnabled(false);
                 }
                 break;
           case "Grade-B":
               if (claimedPastAmnt + apprvAmnt > 500000.00) 
                 {
                 aprrvAmntTextField.setBackground(Color.red);
                 msgJlabel.setText("***Approved amount and past claim amount is exceeding the policy grade limit");
                 apprvBtn.setEnabled(false);
                 }
                 break; 
           case "Grade-C":
               if (claimedPastAmnt + apprvAmnt > 250000.00) 
                 {
                 aprrvAmntTextField.setBackground(Color.red);
                 msgJlabel.setText("***Approved amount and past claim amount is exceeding the policy grade limit");
                 apprvBtn.setEnabled(false);
                 }     
                 break;
           case "Grade-D":
               if (claimedPastAmnt + apprvAmnt > 100000.00) 
                 {
                 aprrvAmntTextField.setBackground(Color.red);
                 msgJlabel.setText("***Approved amount and past claim amount is exceeding the policy grade limit");
                 apprvBtn.setEnabled(false);
                 }  
                 break ;
           case "Grade-E":
               if (claimedPastAmnt + apprvAmnt > 50000.00) 
                 {
                 aprrvAmntTextField.setBackground(Color.red);
                 msgJlabel.setText("***Approved amount and past claim amount is exceeding the policy grade limit");
                 apprvBtn.setEnabled(false);
                 }     
                 break;
       }  
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimAuditorJPanel Auditor aprrvAmntTextFieldFocusLost");
    }//GEN-LAST:event_aprrvAmntTextFieldFocusLost

    private void rejectBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rejectBtnActionPerformed
        // TODO add your handling code here:
        try
        {
            
        log.debug("Entering ProcessClaimAuditorJPanel Auditor rejectBtnActionPerformed");
        if (auditorRemarksTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Auditor Remarks for the Claim cannot be blank!");
            return;
        }
        Remark rem=new Remark();
        rem.setRole("Auditor");
        rem.setUsername(userAccount.getUsername());
        rem.setComment(auditorRemarksTextField.getText());
        rem.setCreationDate(new Date());
        request.getRemarkList().add(rem);
        request.setStatus("Claim Cancelled");        
        request.setResolveDate(new Date());        
        userAccount.getWorkQueue().getWorkRequestList().remove(request);
     //   organization.getWorkQueue().getWorkRequestList().remove(request);
        
        JOptionPane.showMessageDialog(null,"Claim rejected successfully");
        apprvBtn.setEnabled(false);
            rejectBtn.setEnabled(false);
            saveBtn.setEnabled(false);
            sendMail(request,auditorRemarksTextField.getText());
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ProcessClaimAuditorJPanel Auditor rejectBtnActionPerformed");
    }//GEN-LAST:event_rejectBtnActionPerformed

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        // TODO add your handling code here:

        if (auditorRemarksTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Remarks for the Claim cannot be blank!");
            return;
        } else {
            Remark rem = new Remark();
            rem.setRole("Auditor");
            rem.setUsername(userAccount.getUsername());
            rem.setComment(auditorRemarksTextField.getText());
            rem.setStatus("SAVE");
            rem.setCreationDate(new Date());
            request.getRemarkList().add(rem);
            JOptionPane.showMessageDialog(this, "Remarks saved successfully!");
            apprvBtn.setEnabled(false);
            rejectBtn.setEnabled(false);
            saveBtn.setEnabled(false);
        }
    }//GEN-LAST:event_saveBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField agentIDTextField;
    private javax.swing.JTextField agingTextField;
    private javax.swing.JButton apprvBtn;
    private javax.swing.JTextField aprrvAmntTextField;
    private javax.swing.JTextArea auditorRemarksTextField;
    private javax.swing.JButton btnBack;
    private javax.swing.JTextField claiDateTextField;
    private javax.swing.JTextField claimAmntTextField;
    private javax.swing.JTextField claimIDTextField;
    private javax.swing.JTextField claimPastAmntTextField;
    private javax.swing.JTextField claimSatusTextField;
    private javax.swing.JRadioButton hospitaljRadioButton;
    private javax.swing.JTextField hosptalIDTextField;
    private javax.swing.JLabel imagejLabel;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel msgJlabel;
    private javax.swing.JLabel msgjLabel;
    private javax.swing.JRadioButton patientjRadioButton;
    private javax.swing.JTextField policyEndDtextField;
    private javax.swing.JTextField policyGradeTextField;
    private javax.swing.JTextField policyIdTextField;
    private javax.swing.JTextField policyStDtextField;
    private javax.swing.JTextField policyStatusTextField;
    private javax.swing.JButton rejectBtn;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTextField superIdTextField;
    private javax.swing.JTable tblCommHist;
    // End of variables declaration//GEN-END:variables
}
