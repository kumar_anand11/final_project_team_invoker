/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.DoctorAssistantRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AgentOrganization;
import Business.Organization.AuditorOrganization;
import Business.Organization.DoctorAssistantOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HospitalWorkRequest;
import Business.WorkQueue.InsuranceWorkRequest;
import Business.WorkQueue.Remark;
import Business.mailUtility.SendMailUsingAuthentication;
import java.awt.CardLayout;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.mail.MessagingException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Shashank
 */
//Class for Create-Form for Personal-Information application
public class VisitClaimJPanel extends javax.swing.JPanel {

    //Variable Declaration
    private HospitalWorkRequest request;
    //Variable declarations
    private Logger log=Logger.getLogger(VisitClaimJPanel.class);
    private JPanel userProcessContainer;
    private DoctorAssistantOrganization org;
    private Enterprise enterprise;
    private EcoSystem ecosys;
    private UserAccount account;
    private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    private DecimalFormat df2 = new DecimalFormat(".##");
    /**
     * Creates new form createJPanel
     */
    public VisitClaimJPanel(JPanel userProcessContainer, DoctorAssistantOrganization org, Enterprise enterprise, EcoSystem ecosystem, HospitalWorkRequest hwr, UserAccount ua) {
        log.debug("Entering VisitClaimJPanel DoctorAssistant constructor");
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.ecosys=ecosystem;
        this.enterprise=enterprise;
        this.org=org;
        this.request=hwr;
        this.account=ua;
        
        populate();
    
        //add actionListener to DocFees field
        txtDocFees.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                change();
            }

            public void removeUpdate(DocumentEvent e) {
                change();
            }

            public void insertUpdate(DocumentEvent e) {
                change();
            }

            public void change() {
                try {
                    if (txtDocFees.getText().isEmpty() || (txtDocFees.getText().trim().length() == 0)) {
                        txtDocFees.setBorder(null);
                        txtDocFees.updateUI();        
                    } else{
                       double totalBill = Double.parseDouble(txtDocFees.getText())+request.getTotalBill();
                        double tax = (0.06 * totalBill);
                        totalBill += tax;
                        txtTotalAmt.setText(String.valueOf(df2.format(totalBill)));
                        txtTax.setText(String.valueOf(df2.format(tax))); 
                    }
                } catch (NumberFormatException nfe) {
                    System.out.println("Number-Format-Exception occurred: " + nfe.getMessage());
                    txtDocFees.setBorder(new LineBorder(Color.RED, 2));
                } catch(Exception e){
                    System.out.println("Exception occurred: " + e.getMessage());
                }
            }
        });
        log.debug("Exiting VisitClaimJPanel DoctorAssistant constructor");
    }
    
    public String getDoctorName(String docID) {
        String name = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization instanceof DoctorOrganization) {
                for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                    if (ua.getUsername().equals(docID)) {
                        name = ua.getEmployee().getFirstName() + " " + ua.getEmployee().getLastName();
                        break;
                    }
                }
            }
        }
        return name;
    }
    
    public String getAssistantName(String assistID) {
        String name = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization instanceof DoctorAssistantOrganization) {
                for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                    if (ua.getUsername().equals(assistID)) {
                        name = ua.getEmployee().getLastName() + ", " + ua.getEmployee().getFirstName();
                        break;
                    }
                }
            }
        }
        return name;
    }
    
    public void populate() {
        //Patient
        txtVisitID.setText(request.getVisitID());
        txtPatientID.setText(request.getPatient().getPatientID());
        txtPatientName.setText((request.getPatient().getLastName()+ ", " + request.getPatient().getFirstName()));
        txtDob.setText(sf.format(request.getPatient().getDob()));
        txtPhone.setText(request.getPatient().getPhoneNum());
        txtEmail.setText(request.getPatient().geteMail());
        txtDocName.setText(getDoctorName(request.getDoctorID()));
        txtAssistantID.setText(getDoctorName(request.getDoctorID()));
        txtTypeVisit.setText(request.getPatient().getNatureOfVisit());
        
        //Policy
        txtPolicyID.setText(request.getPatient().getPolicy().getPolicyID());
        txtStartDate.setText(sf.format(request.getPatient().getPolicy().getStartDate()));
        txtEndDate.setText(sf.format(request.getPatient().getPolicy().getEndDate()));
        txtGrade.setText(request.getPatient().getPolicy().getPolicyGrade());
        txtPolHoldName.setText((request.getPatient().getLastName()+ ", " + request.getPatient().getFirstName()));
        txtInsurComp.setText(request.getPatient().getPolicy().getInsuranceComp());
        
        //Bill
        txtDocFees.setText("");
        txtLabFees.setText(String.valueOf(request.getLabFees()));
        txtPharmaCharges.setText(String.valueOf(request.getMedicationAmt()));
        double totalBill=request.getLabFees()+request.getMedicationAmt();
        request.setTotalBill(totalBill);
        double tax=(0.06*totalBill);
        totalBill+=tax;
        request.setTax(Double.parseDouble(df2.format(tax)));
        request.setTotalWithTax(Double.parseDouble(df2.format(totalBill)));
        txtTotalAmt.setText(String.valueOf(request.getTotalWithTax()));
        txtTax.setText(String.valueOf(request.getTax()));
    }
    
    public String getDoctorEmail(String docID) {
        String mailID = null;
        for (Network network : ecosys.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("hospital")) {
                    for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (organization instanceof DoctorOrganization) {
                            for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                if (ua.getUsername().equals(docID)) {
                                    mailID = ua.getEmployee().getEmail();
                                    break;
                                }
                            }
                        }
                    }

                }
            }
        }
        return mailID;
    }
    
    public String getAuditorEmail() {
        String mailID = null;
        for (Network network : ecosys.getNetworkList()) {
                for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (ent.getEnterpriseType().getValue().equalsIgnoreCase("insurance")) {
                        if (request.getPatient().getPolicy().getInsuranceComp().equals(ent.getName())) {
                            for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                                if (organization instanceof AuditorOrganization) {
                                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                        if(ua.getEmployee().getHospitalAssignment().equals(enterprise.getName())){
                                            mailID=ua.getEmployee().getEmail();
                                            break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        return mailID;
    }
    
    public String getAuditorName() {
        String name = null;
        for (Network network : ecosys.getNetworkList()) {
                for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (ent.getEnterpriseType().getValue().equalsIgnoreCase("insurance")) {
                        if (request.getPatient().getPolicy().getInsuranceComp().equals(ent.getName())) {
                            for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                                if (organization instanceof AuditorOrganization) {
                                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                        if(ua.getEmployee().getHospitalAssignment().equals(enterprise.getName())){
                                            name=ua.getEmployee().getFirstName()+" "+ua.getEmployee().getLastName();
                                            break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        return name;
    }
    
    
    public void sendMail(HospitalWorkRequest hwr, InsuranceWorkRequest iwr) {
        try
        {
            log.debug("Entering VisitClaimJPanel DoctorAssistant sendMail");
            String emailMsgTxt = "";
            String emailSubjectTxt = "Thanks for your recent visit to "+hwr.getHospitalID()+"!";
            String emailFromAddress = SendMailUsingAuthentication.SMTP_AUTH_USER;
            String[] emailList = new String[1];
            hwr.setHospitalID(enterprise.getName());

            SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();
            try {
                //Patient Mail
                emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Mr/s. "+hwr.getPatient().getFirstName()+",<br>  Thanks for visiting "+hwr.getHospitalID()+". Please find below the details of your visit:<br> <br><br> <b><i> Visit-ID: "+hwr.getVisitID()+"<br> "
                        + "Date-Of-Visit: "+sf.format(hwr.getRequestDate())+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                        + "Doctor's Name: "+getDoctorName(hwr.getDoctorID())+"<br> Total-Amount: $ "+hwr.getTotalWithTax()+"<br> </i></b> <br><br> "
                        + "The following claim has been raised on your behalf for this visit:<br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> Insurance-Company: "+iwr.getInsuranceCompID()+"<br> Policy-ID: "+iwr.getPolicy().getPolicyID()+"<br> Claim-Amount: $ "+iwr.getClaimAmnt()+" <br> </i></b> "
                        + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+hwr.getHospitalID()+" Team<br> <HR> </BODY> </HTML>";
                hwr.setHospitalID(enterprise.getName());
                emailList[0]=hwr.getPatient().geteMail();
                smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);

                //Doctor Mail
                emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Dr. "+getDoctorName(hwr.getDoctorID())+",<br>  Hope this mail finds you in good health. This is to inform you that a claim has been raised by on behalf of your patient Mr/s.  "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+" for his recent visit to "+hwr.getHospitalID()+"."
                        + "<br> Please find below the details claim:<br> <br><br> <b><i> Visit-ID: "+hwr.getVisitID()+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> "
                        + "Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                        + "Doctor's Name: "+getDoctorName(hwr.getDoctorID())+"<br> Total-Amount: $ "+hwr.getTotalWithTax()+"<br> </i></b> <br><br> "
                        + "<br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> Insurance-Company: "+iwr.getInsuranceCompID()+"<br> Policy-ID: "+iwr.getPolicy().getPolicyID()+"<br> Claim-Amount: $ "+iwr.getClaimAmnt()+" <br> </i></b> "
                        + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+hwr.getHospitalID()+" Team<br> <HR> </BODY> </HTML>";

                emailList[0]=getDoctorEmail(hwr.getDoctorID());
                emailSubjectTxt="Claim raised with "+iwr.getInsuranceCompID()+" by "+hwr.getHospitalID()+"!";
                smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);

                String auditorName=getAuditorName();
                String auditorID=getAuditorEmail();
                emailSubjectTxt="Claim raised by "+hwr.getHospitalID()+"!";
                emailMsgTxt = "<HTML>  <BODY BGCOLOR=\"FFFFFF\">  <HR>  Dear Mr/s. "+auditorName+",<br>  Hope this mail finds you in good health. This is to inform you that a claim has been raised by "+hwr.getHospitalID()+"."
                        + "<br> Please find below the details claim:<br> <br><br> <b><i> Visit-ID: "+hwr.getVisitID()+"<br> Patient-ID: "+hwr.getPatient().getPatientID()+"<br> "
                        + "Patient-Name: "+hwr.getPatient().getFirstName()+" "+hwr.getPatient().getLastName()+"<br> "
                        + "Doctor's Name: "+getDoctorName(hwr.getDoctorID())+"<br> Total-Amount: $ "+hwr.getTotalWithTax()+"<br> </i></b> <br><br> "
                        + "<br> <b><i> Claim-ID: "+iwr.getClaimID()+"<br> Insurance-Company: "+iwr.getInsuranceCompID()+"<br> Policy-ID: "+iwr.getPolicy().getPolicyID()+" <br> </i></b> "
                        + "<br> In case of any issues/doubts, please contact us at mediassist.testuser777@gmail.com.<br><br>  Regards,<br> "+hwr.getHospitalID()+" Team<br> <HR> </BODY> </HTML>";

                emailList[0]=auditorID;
                smtpMailSender.postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);


            } catch (MessagingException ex) {
                ex.printStackTrace();
            }
            System.out.println("Sucessfully Sent mail to All Users");
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting VisitClaimJPanel DoctorAssistant sendMail");
}

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLbl = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        streetAddrLbl = new javax.swing.JLabel();
        txtDocFees = new javax.swing.JTextField();
        cityLbl = new javax.swing.JLabel();
        txtLabFees = new javax.swing.JTextField();
        zipLbl = new javax.swing.JLabel();
        txtTotalAmt = new javax.swing.JTextField();
        zipLbl1 = new javax.swing.JLabel();
        txtPharmaCharges = new javax.swing.JTextField();
        zipLbl2 = new javax.swing.JLabel();
        txtTax = new javax.swing.JTextField();
        streetAddrLbl2 = new javax.swing.JLabel();
        streetAddrLbl3 = new javax.swing.JLabel();
        streetAddrLbl4 = new javax.swing.JLabel();
        streetAddrLbl1 = new javax.swing.JLabel();
        streetAddrLbl5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        svBnkNamLbl = new javax.swing.JLabel();
        txtPolicyID = new javax.swing.JTextField();
        svBnkRoutngNumLbl1 = new javax.swing.JLabel();
        svBnkNamLbl4 = new javax.swing.JLabel();
        txtPolHoldName = new javax.swing.JTextField();
        svBnkAccBalLbl = new javax.swing.JLabel();
        txtGrade = new javax.swing.JTextField();
        svBnkRoutngNumLbl = new javax.swing.JLabel();
        txtStartDate = new javax.swing.JTextField();
        txtEndDate = new javax.swing.JTextField();
        svBnkAccBalLbl1 = new javax.swing.JLabel();
        txtInsurComp = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        lblVisitId = new javax.swing.JLabel();
        txtVisitID = new javax.swing.JTextField();
        lblVisitId1 = new javax.swing.JLabel();
        txtPatientID = new javax.swing.JTextField();
        lblVisitId2 = new javax.swing.JLabel();
        txtPatientName = new javax.swing.JTextField();
        lblVisitId3 = new javax.swing.JLabel();
        txtDob = new javax.swing.JTextField();
        lblVisitId4 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        lblVisitId5 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        txtDocName = new javax.swing.JTextField();
        txtAssistantID = new javax.swing.JTextField();
        lblVisitId6 = new javax.swing.JLabel();
        txtTypeVisit = new javax.swing.JTextField();
        lblVisitId7 = new javax.swing.JLabel();
        lblVisitId8 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        btnRaiseClaim = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 153, 153));

        titleLbl.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        titleLbl.setText("RAISE CLAIM FORM");

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Visit Bill", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel2.setMinimumSize(new java.awt.Dimension(50, 50));

        streetAddrLbl.setText("Doctor-Fees: ");

        cityLbl.setText("Lab-Fees:");

        txtLabFees.setEditable(false);

        zipLbl.setText("Total-Amount:");

        txtTotalAmt.setEditable(false);

        zipLbl1.setText("Pharmacy-Charges:");

        txtPharmaCharges.setEditable(false);

        zipLbl2.setText("Tax (@6%) :");

        txtTax.setEditable(false);

        streetAddrLbl2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        streetAddrLbl2.setText("$");

        streetAddrLbl3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        streetAddrLbl3.setText("$");

        streetAddrLbl4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        streetAddrLbl4.setText("$");

        streetAddrLbl1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        streetAddrLbl1.setText("$");

        streetAddrLbl5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        streetAddrLbl5.setText("$");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(streetAddrLbl)
                        .addGap(50, 50, 50)
                        .addComponent(streetAddrLbl2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocFees, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(zipLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(streetAddrLbl5))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(zipLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(streetAddrLbl1))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(zipLbl1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                                .addComponent(streetAddrLbl4))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(cityLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(streetAddrLbl3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTax, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtPharmaCharges, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                                .addComponent(txtLabFees))
                            .addComponent(txtTotalAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(35, 35, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(streetAddrLbl)
                    .addComponent(txtDocFees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetAddrLbl2))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cityLbl)
                    .addComponent(txtLabFees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetAddrLbl3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(zipLbl1)
                    .addComponent(txtPharmaCharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetAddrLbl4))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(zipLbl2)
                    .addComponent(txtTax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetAddrLbl1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zipLbl)
                    .addComponent(streetAddrLbl5))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Policy Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel3.setMinimumSize(new java.awt.Dimension(50, 50));

        svBnkNamLbl.setText("Policy-ID:");

        txtPolicyID.setEditable(false);

        svBnkRoutngNumLbl1.setText("Start-Date:");

        svBnkNamLbl4.setText("Policy-Holder's Name:");

        txtPolHoldName.setEditable(false);

        svBnkAccBalLbl.setText("Policy-Grade:");

        txtGrade.setEditable(false);

        svBnkRoutngNumLbl.setText("End-Date:");

        txtStartDate.setEditable(false);

        txtEndDate.setEditable(false);

        svBnkAccBalLbl1.setText("Company:");

        txtInsurComp.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl)
                    .addComponent(svBnkRoutngNumLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolicyID, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(txtStartDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl4)
                    .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolHoldName)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 129, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkAccBalLbl)
                    .addComponent(svBnkAccBalLbl1))
                .addGap(39, 39, 39)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtInsurComp, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGrade, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(svBnkAccBalLbl1)
                        .addComponent(txtInsurComp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(svBnkNamLbl)
                        .addComponent(txtPolicyID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(svBnkNamLbl4)
                        .addComponent(txtPolHoldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(svBnkAccBalLbl)
                        .addComponent(txtGrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(svBnkRoutngNumLbl1)
                        .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(0, 153, 153));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Patient Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jPanel7.setMinimumSize(new java.awt.Dimension(50, 50));

        lblVisitId.setText("Visit-ID:");

        txtVisitID.setEditable(false);

        lblVisitId1.setText("Patient-ID:");

        txtPatientID.setEditable(false);

        lblVisitId2.setText("Patient-Name:");

        txtPatientName.setEditable(false);

        lblVisitId3.setText("DOB:");

        txtDob.setEditable(false);

        lblVisitId4.setText("Phone:");

        txtPhone.setEditable(false);

        lblVisitId5.setText("Email:");

        txtEmail.setEditable(false);

        txtDocName.setEditable(false);

        txtAssistantID.setEditable(false);

        lblVisitId6.setText("Assistant-Name:");

        txtTypeVisit.setEditable(false);

        lblVisitId7.setText("Type of Visit:");

        lblVisitId8.setText("Doctor-Name:");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblVisitId8)
                    .addComponent(lblVisitId3)
                    .addComponent(lblVisitId))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtVisitID, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtDob)
                        .addComponent(txtDocName, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 117, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblVisitId6)
                    .addComponent(lblVisitId4)
                    .addComponent(lblVisitId1))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPatientID, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtAssistantID)
                        .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 117, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblVisitId2)
                    .addComponent(lblVisitId7)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(lblVisitId5)))
                .addGap(43, 43, 43)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPatientName)
                    .addComponent(txtEmail)
                    .addComponent(txtTypeVisit, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblVisitId)
                            .addComponent(txtVisitID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId1)
                            .addComponent(txtPatientID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblVisitId3)
                            .addComponent(txtDob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId4)
                            .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblVisitId8)
                            .addComponent(txtDocName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId6)
                            .addComponent(txtAssistantID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPatientName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId2))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId5))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTypeVisit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVisitId7))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 250, 205));
        jPanel8.setOpaque(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 64, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 611, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 250, 205));
        jPanel9.setOpaque(false);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 25, Short.MAX_VALUE)
        );

        btnBack.setText("<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnRaiseClaim.setText("Raise Claim");
        btnRaiseClaim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRaiseClaimActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnBack)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnRaiseClaim))
                                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(titleLbl)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(titleLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack)
                    .addComponent(btnRaiseClaim))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
    }// </editor-fold>//GEN-END:initComponents

    //Method to set the values from screen-fields to respective object-attributes
    private void btnRaiseClaimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRaiseClaimActionPerformed
        // TODO add your handling code here:
        //Validations
        try{
            log.debug("Entering VisitClaimJPanel DoctorAssistant btnRaiseClaimActionPerformed");
        try {
            if (txtDocFees.getText().isEmpty() || txtDocFees.getText().trim().length() == 0) {
                txtDocFees.setBorder(new LineBorder(Color.RED, 2));
                JOptionPane.showMessageDialog(this, "Please enter the Doctor's Fees!");
                return;
            } else {
                request.setDocFees(Double.parseDouble(df2.format(Double.parseDouble(txtDocFees.getText()))));
                txtDocFees.setBorder(null);
                txtDocFees.updateUI();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Number-Format-Exception occurred: " + nfe.getMessage());
            txtDocFees.setBorder(new LineBorder(Color.RED, 2));
            JOptionPane.showMessageDialog(this, "Please enter the Doctor's Fees!");
            return;
        }

        double totalBill = Double.parseDouble(txtDocFees.getText()) + request.getTotalBill();
        request.setTotalBill(Double.parseDouble(df2.format(totalBill)));
        double tax = (0.06 * totalBill);
        totalBill += tax;
        //txtTotalAmt.setText(String.valueOf(totalBill));
        //txtTax.setText(String.valueOf(tax));
        String billValue=df2.format(totalBill);

        int dialogRes = JOptionPane.showConfirmDialog(this, "Are you sure you want to raise a claim of $" + billValue + " for Visit-ID: " + request.getVisitID() + "?");
        if (dialogRes == JOptionPane.YES_OPTION) {
            request.setTotalBill(Double.parseDouble(df2.format(totalBill - tax)));
            request.setTax(Double.parseDouble(df2.format(tax)));
            request.setTotalWithTax(Double.parseDouble(df2.format(totalBill)));
            request.setDocFees(Double.parseDouble(df2.format(Double.parseDouble(txtDocFees.getText()))));
            InsuranceWorkRequest iwr = new InsuranceWorkRequest();
            iwr.setHospitalReq(request);
            iwr.setPolicy(request.getPatient().getPolicy());
            iwr.setInsuranceCompID(request.getPatient().getPolicy().getInsuranceComp());
            iwr.setClaimAmnt(request.getTotalWithTax());
            iwr.setStatus("New");
            Remark remark = new Remark();
            remark.setCreationDate(new Date());
            remark.setRole("-");
            remark.setUsername("System");
            remark.setComment("Claim request created.");
            iwr.getRemarkList().add(remark);
            request.setStatus("Closed");
            request.setClaimID(iwr.getClaimID());

            //Select auditor for request
            for (Network network : ecosys.getNetworkList()) {
                for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (ent.getEnterpriseType().getValue().equalsIgnoreCase("insurance")) {
                        if (request.getPatient().getPolicy().getInsuranceComp().equals(ent.getName())) {
                            for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                                if (organization instanceof AuditorOrganization) {
                                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                        if(ua.getEmployee().getHospitalAssignment().equals(enterprise.getName())){
                                            System.out.println("Auditor-Name :" + ua.getUsername());
                                            iwr.setAuditorID(ua.getUsername());
                                            break;
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Add this Work-Request to queue
            Organization agentOrg = null;
            for (Network network : ecosys.getNetworkList()) {
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (enterprise.getEnterpriseType().getValue().equalsIgnoreCase("insurance")) {
                        if (request.getPatient().getPolicy().getInsuranceComp().equals(enterprise.getName())) {
                            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                                if (organization instanceof AgentOrganization) {
                                    agentOrg = organization;
                                    System.out.println("name : " + agentOrg.getName());
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (agentOrg != null) {
                agentOrg.getWorkQueue().getWorkRequestList().add(iwr);
                sendMail(request,iwr);
            }

            System.out.println("Agent Que:" + agentOrg.getWorkQueue().getWorkRequestList().size());

            //display confirmation message
            System.out.println("HWR: " + request);
            JOptionPane.showMessageDialog(this, "Claim raised Successfully!");
            btnRaiseClaim.setEnabled(false);
            //JOptionPane.showMessageDialog(this, "details: "+iwr.getClaimID()+"\n"+iwr.getStatus()+"\n"+iwr.getHospitalReq().getPatient().getPatientID());
            populate();
        }
        
        System.out.println("hwr: "+request.toString());
    }
    catch(Exception e)
    {
        log.error(e.getMessage());
        
    }
        log.debug("Exiting VisitClaimJPanel DoctorAssistant btnRaiseClaimActionPerformed");
        
    }//GEN-LAST:event_btnRaiseClaimActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtDocFeesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDocFeesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDocFeesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnRaiseClaim;
    private javax.swing.JLabel cityLbl;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JLabel lblVisitId;
    private javax.swing.JLabel lblVisitId1;
    private javax.swing.JLabel lblVisitId2;
    private javax.swing.JLabel lblVisitId3;
    private javax.swing.JLabel lblVisitId4;
    private javax.swing.JLabel lblVisitId5;
    private javax.swing.JLabel lblVisitId6;
    private javax.swing.JLabel lblVisitId7;
    private javax.swing.JLabel lblVisitId8;
    private javax.swing.JLabel streetAddrLbl;
    private javax.swing.JLabel streetAddrLbl1;
    private javax.swing.JLabel streetAddrLbl2;
    private javax.swing.JLabel streetAddrLbl3;
    private javax.swing.JLabel streetAddrLbl4;
    private javax.swing.JLabel streetAddrLbl5;
    private javax.swing.JLabel svBnkAccBalLbl;
    private javax.swing.JLabel svBnkAccBalLbl1;
    private javax.swing.JLabel svBnkNamLbl;
    private javax.swing.JLabel svBnkNamLbl4;
    private javax.swing.JLabel svBnkRoutngNumLbl;
    private javax.swing.JLabel svBnkRoutngNumLbl1;
    private javax.swing.JLabel titleLbl;
    private javax.swing.JTextField txtAssistantID;
    private javax.swing.JTextField txtDob;
    private javax.swing.JTextField txtDocFees;
    private javax.swing.JTextField txtDocName;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEndDate;
    private javax.swing.JTextField txtGrade;
    private javax.swing.JTextField txtInsurComp;
    private javax.swing.JTextField txtLabFees;
    private javax.swing.JTextField txtPatientID;
    private javax.swing.JTextField txtPatientName;
    private javax.swing.JTextField txtPharmaCharges;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPolHoldName;
    private javax.swing.JTextField txtPolicyID;
    private javax.swing.JTextField txtStartDate;
    private javax.swing.JTextField txtTax;
    private javax.swing.JTextField txtTotalAmt;
    private javax.swing.JTextField txtTypeVisit;
    private javax.swing.JTextField txtVisitID;
    private javax.swing.JLabel zipLbl;
    private javax.swing.JLabel zipLbl1;
    private javax.swing.JLabel zipLbl2;
    // End of variables declaration//GEN-END:variables
}
