/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HospitalAdmin;

import Business.Employee.AccountDetails;
import Business.Organization.OrganizationDirectory;
import Business.Organization.*;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;


public class ManageEmployeeJPanel extends javax.swing.JPanel {
    private Logger log=Logger.getLogger(ManageEmployeeJPanel.class);
    private OrganizationDirectory organizationDir;
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount account;
    private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    /**
     * Creates new form ManageOrganizationJPanel
     */
    
    public ManageEmployeeJPanel(JPanel userProcessContainer,UserAccount account, OrganizationDirectory organizationDir, Enterprise enterprise) {
        log.debug("Entering Hospital Admin ManageEmployeeJPanel constructor");
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organizationDir = organizationDir;
        this.enterprise=enterprise;
        this.account=account;
        userjLabel.setText("Welcome" + " " +account.getEmployee().getFirstName());
        populateOrganizationComboBox(); 
        
        log.debug("Exiting Hospital Admin ManageEmployeeJPanel constructor");
        
    }

    public void populateOrganizationComboBox(){
        organizationJComboBox.removeAllItems();
        organizationEmpJComboBox.removeAllItems();
        
        for (Organization organization : organizationDir.getOrganizationList()){
            organizationJComboBox.addItem(organization);
            organizationEmpJComboBox.addItem(organization);
        }
    }
    
    public void populateOrganizationEmpComboBox(){
        organizationEmpJComboBox.removeAllItems();
        
        for (Organization organization : organizationDir.getOrganizationList()){
            organizationEmpJComboBox.addItem(organization);
        }
 
        
    }

    private void populateTable(Organization organization){
        try{
        log.debug("Entering Hospital Admin ManageEmployeeJPanel populateTable");
        DefaultTableModel model = (DefaultTableModel) organizationJTable.getModel();
        
        model.setRowCount(0);
        
        for (Employee employee : organization.getEmployeeDirectory().getEmployeeList()){
            Object[] row = new Object[4];
            row[0] = employee.getEmpNo();
            row[1] = employee.getFirstName()+" "+employee.getLastName();
            row[2] = employee.getDesignation();
            row[3] = sf.format(employee.getDateofJoining());
             model.addRow(row);
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting Hospital Admin ManageEmployeeJPanel populateTable");
        
    }
    public boolean validateLetters(String txt) {
        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validatePhone(String txt) {
        String regx = "\\d{10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateEmpNo(String txt) {
        String regx = "\\d{1,10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateEmail(String txt) {
        String regx = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateLettersAndDigits(String txt) {
        String regx = "^[a-zA-Z0-9\\s.,-]*$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateNumber(String txt) {
        String regx = "\\d{10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateNumber1(String txt) {
        String regx = "\\d{1,10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
     private void repopulate() 
    {
        try{
        log.debug("Entering Hospital Admin ManageEmployeeJPanel repopulate");
        Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
        int selectedRow = organizationJTable.getSelectedRow();
        if(selectedRow<0)
        {
            JOptionPane.showMessageDialog(this, "Please select a row!!");
            return;
        }
        String id = (String)organizationJTable.getValueAt(selectedRow, 0);
        System.out.println("ID: "+id);
        Employee emp=null;
        Organization empOrg = null;
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
               for (Employee employee : org.getEmployeeDirectory().getEmployeeList()) {
                   if (employee.getEmpNo().equals(id)) {
                       emp = employee;
                       empOrg=org;
                       break;
                   }
               }
           }

           if(null==emp || null==empOrg){
             JOptionPane.showMessageDialog(this, "No such employee exists!!");
           }
           else
           {
                if(((Organization)organizationJComboBox.getSelectedItem()).getName().equals("Doctor Organization"))
               {
                    
                    firstnameJTextField.setText(emp.getFirstName());
                    lastjTextField.setText(emp.getLastName());
                    empNojTextField.setText(emp.getEmpNo());
                    designationjTextField.setText(emp.getDesignation());
                    deptjComboBox.setSelectedItem(emp.getDepartment());
                    specializationjTextField.setText(emp.getSpecialization());
                    emailnameJTextField.setText(emp.getEmail()) ;
                    phonejTextField.setText(emp.getPhone()) ;
                    txtDob.setDate(emp.getDateofJoining());
                    organizationEmpJComboBox.setSelectedItem(empOrg.getName());
                    bankNamejTextField.setText(emp.getAccountDetl().getAccountName());
                    accNojTextField.setText(emp.getAccountDetl().getAccountNumber());
                    routingNumjTextField.setText(emp.getAccountDetl().getRoutingNumber());
                    
                    System.out.println(emp.getAccountDetl().getAccountName()+
                    emp.getAccountDetl().getAccountNumber()+
                    emp.getAccountDetl().getRoutingNumber());
                    organizationEmpJComboBox.setEnabled(false);
                    accNojTextField.setEnabled(false);
                    bankNamejTextField.setEnabled(false);
                    routingNumjTextField.setEnabled(false);
               }
                else
                {
                    firstnameJTextField.setText(emp.getFirstName());
                    lastjTextField.setText(emp.getLastName());
                    empNojTextField.setText(emp.getEmpNo());
                    designationjTextField.setText(emp.getDesignation());
                    deptjComboBox.setSelectedItem(emp.getDepartment());
                    specializationjTextField.setText(emp.getSpecialization());
                    emailnameJTextField.setText(emp.getEmail()) ;
                    phonejTextField.setText(emp.getPhone()) ;
                    txtDob.setDate(emp.getDateofJoining());
                    organizationEmpJComboBox.setSelectedItem(empOrg);
                    organizationEmpJComboBox.setEnabled(false);
                    empNojTextField.setEnabled(false);
                    accNojTextField.setEnabled(false);
                    bankNamejTextField.setEnabled(false);
                    routingNumjTextField.setEnabled(false);
                }
   
           }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting Hospital Admin ManageEmployeeJPanel repopulate");
    }
    
   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        organizationJTable = new javax.swing.JTable();
        addJButton = new javax.swing.JButton();
        organizationJComboBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        backJButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        firstnameJTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        designationjTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        deptjComboBox = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        specializationjTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        emailnameJTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        phonejTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtDob = new com.toedter.calendar.JDateChooser();
        jLabel16 = new javax.swing.JLabel();
        empNojTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        organizationEmpJComboBox = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        lastjTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        bankNamejTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        accNojTextField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        routingNumjTextField = new javax.swing.JTextField();
        updatejButton = new javax.swing.JButton();
        canceljButton = new javax.swing.JButton();
        viewjButton = new javax.swing.JButton();
        userjLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 153, 153));

        organizationJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Employee-Number", "Employee-Name", "Designation", "Date of Joining"
            }
        ));
        jScrollPane1.setViewportView(organizationJTable);
        if (organizationJTable.getColumnModel().getColumnCount() > 0) {
            organizationJTable.getColumnModel().getColumn(0).setResizable(false);
            organizationJTable.getColumnModel().getColumn(1).setResizable(false);
        }

        addJButton.setText("Create Employee");
        addJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addJButtonActionPerformed(evt);
            }
        });

        organizationJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationJComboBoxActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Organization - Type :");

        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel7.setText("Add Employees To Organization");

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("EMPLOYEE DETAILS"));

        jLabel2.setText("First Name :");

        jLabel10.setText("Designation :");

        jLabel4.setText("Department :");

        deptjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cardiology", "Dermatology", "Optometry", "General Surgery", "Virology", "Neurosurgery", "Orthopedics", "Oncology", "General Physician" }));

        jLabel8.setText("Specialization :");

        jLabel5.setText("Email :");

        jLabel9.setText("Phone Number :");

        jLabel6.setText("Date Of Joining :");

        txtDob.setDateFormatString("MM/dd/yyyy");

        jLabel16.setText("Employee No :");

        jLabel3.setText("Organization :");

        organizationEmpJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationEmpJComboBox.setActionCommand("");
        organizationEmpJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationEmpJComboBoxActionPerformed(evt);
            }
        });

        jLabel17.setText("Last Name :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel16)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6))
                .addGap(71, 71, 71)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(firstnameJTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                            .addComponent(emailnameJTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                            .addComponent(empNojTextField)
                            .addComponent(organizationEmpJComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(122, 122, 122)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)))
                    .addComponent(txtDob, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(phonejTextField)
                    .addComponent(specializationjTextField, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lastjTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                .addGap(107, 107, 107)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(designationjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deptjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(organizationEmpJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(firstnameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(designationjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(lastjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(specializationjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(empNojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(deptjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(phonejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(emailnameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("BANK ACCOUNT DETAILS\n"));

        jLabel12.setText("Bank Account Name :");

        jLabel13.setText("Bank Account Number :");

        jLabel14.setText("Bank Routing Number :");

        routingNumjTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                routingNumjTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addGap(30, 30, 30)
                .addComponent(bankNamejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(jLabel13)
                .addGap(33, 33, 33)
                .addComponent(accNojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addGap(33, 33, 33)
                .addComponent(routingNumjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(bankNamejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(accNojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(routingNumjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        updatejButton.setText("Update Employee");
        updatejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatejButtonActionPerformed(evt);
            }
        });

        canceljButton.setText("Cancel");
        canceljButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                canceljButtonActionPerformed(evt);
            }
        });

        viewjButton.setText("View");
        viewjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewjButtonActionPerformed(evt);
            }
        });

        userjLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backJButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addJButton)
                        .addGap(151, 151, 151)
                        .addComponent(updatejButton)
                        .addGap(147, 147, 147)
                        .addComponent(canceljButton))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(36, 36, 36)
                            .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(userjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(viewjButton)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1016, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(247, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(31, 31, 31)
                .addComponent(userjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewjButton)
                .addGap(44, 44, 44)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(canceljButton)
                    .addComponent(updatejButton)
                    .addComponent(addJButton)
                    .addComponent(backJButton))
                .addContainerGap(209, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addJButtonActionPerformed
        try{
        log.debug("Entering Hospital Admin ManageEmployeeJPanel addJButtonActionPerformed");
        Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
        if(organization.getName().equals("Doctor Organization"))
        {
            String firstname = firstnameJTextField.getText();
            if(firstnameJTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter First Name!");
                return;
            }
            String lastName = lastjTextField.getText();
            String designation = designationjTextField.getText();
            String specialization = specializationjTextField.getText();
            String email = emailnameJTextField.getText();
            if(emailnameJTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Email!");
                return;
            }
            validateEmail(email);
            String phone = phonejTextField.getText();
            if(phonejTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Phone Number!");
                return;
            }
            else if(phonejTextField.getText().length() !=10)
            {
                JOptionPane.showMessageDialog(null, "Enter a valid Phone Number!");
                return;   
            }
            
            Date doj = (Date) txtDob.getDate();
            String department = String.valueOf(deptjComboBox.getSelectedItem());
            String bankName = bankNamejTextField.getText();
            String bankNumber = accNojTextField.getText();
            String routingNumber = routingNumjTextField.getText();
            if(bankNamejTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Bank Name!");
                return;
            }
            if(accNojTextField.getText().equals(""))
            {
                JOptionPane.showMessageDialog(null, "Enter Bank Number");
                return;
            }
            validateNumber(bankNumber);
            if(routingNumjTextField.getText().equals(""))
            {
                JOptionPane.showMessageDialog(null, "Enter Bank Routing Number");
                return;
            }
            String empNo = empNojTextField.getText();
            if(empNojTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Employee Number!");
                return;  
            }
            validateNumber1(empNo);
            for (Employee employee : organization.getEmployeeDirectory().getEmployeeList()) {
                if (employee.getEmpNo().equals(empNo)) 
                {
                    JOptionPane.showMessageDialog(null, "Emp Id already used, try something else");
                     return; 
                }
            }
            AccountDetails acc= new AccountDetails();
            acc.setAccountName(bankName);
            acc.setAccountNumber(bankNumber);
            acc.setRoutingNumber(routingNumber);
            
            organization.getEmployeeDirectory().createEmployee(firstname, lastName, empNo, specialization, department, email, phone, doj, designation, acc);
            
            populateTable(organization);
        }
        else
        {
            String firstname = firstnameJTextField.getText();
            if(firstnameJTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Name!");
                return;
            }
            String lastName = lastjTextField.getText();
            String designation = designationjTextField.getText();
            String specialization = specializationjTextField.getText();
            String email = emailnameJTextField.getText();
            validateEmail(email);
            if(emailnameJTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Email!");
                return;
            }
            
            
            String phone = phonejTextField.getText();
            validateNumber(phone);
            if(phonejTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Phone Number!");
                return;
            }
            else if(phonejTextField.getText().length() !=10)
            {
                JOptionPane.showMessageDialog(null, "Enter a valid Phone Number!");
                return;   
            }
            
            String empNo = empNojTextField.getText();
            if(empNojTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(null, "Enter Employee Number!");
                return;  
            }
            validateNumber1(empNo);
               for (Employee employee : organization.getEmployeeDirectory().getEmployeeList()) {
                   if (employee.getEmpNo().equals(empNo)) 
                   {
                       JOptionPane.showMessageDialog(null, "Emp Id already used, try something else");
                        return; 
                   }
               }
           
            
            
            Date doj = (Date) txtDob.getDate();
            String department = String.valueOf(deptjComboBox.getSelectedItem());
            
            organization.getEmployeeDirectory().createEmployee(firstname,lastName, empNo,specialization,department,email,phone,doj,designation);
            
            populateTable(organization);
        }
   
        JOptionPane.showMessageDialog(null, "Employee created successfully");
        firstnameJTextField.setText("");
        lastjTextField.setText("");
        designationjTextField.setText("");
        specializationjTextField.setText("");
        emailnameJTextField.setText("");
        phonejTextField.setText("");
        txtDob.setDate(null);
        empNojTextField.setText("");
        bankNamejTextField.setText("");
        accNojTextField.setText("");
        routingNumjTextField.setText("");
        
        if(organization.getName().equals("Doctor Organization"))
        {
            accNojTextField.setEnabled(true);
            bankNamejTextField.setEnabled(true);
            routingNumjTextField.setEnabled(true);
        }
        else
        {
            accNojTextField.setEnabled(false);
            bankNamejTextField.setEnabled(false);
            routingNumjTextField.setEnabled(false);
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting Hospital Admin ManageEmployeeJPanel addJButtonActionPerformed");
    }//GEN-LAST:event_addJButtonActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
    }//GEN-LAST:event_backJButtonActionPerformed

    private void organizationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationJComboBoxActionPerformed
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        if (organization != null){
            populateTable(organization);
        }
    }//GEN-LAST:event_organizationJComboBoxActionPerformed

    private void routingNumjTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_routingNumjTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_routingNumjTextFieldActionPerformed

    private void organizationEmpJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationEmpJComboBoxActionPerformed
        // TODO add your handling code here:
        Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
        if (organization== null)
        {
            
                
                    accNojTextField.setEnabled(true);
                    bankNamejTextField.setEnabled(true);
                    routingNumjTextField.setEnabled(true);
                
            
        }
        else if(organization.getName().equals("Doctor Organization"))
        {
            accNojTextField.setEnabled(true);
            bankNamejTextField.setEnabled(true);
            routingNumjTextField.setEnabled(true);
        }
        else
        {
            accNojTextField.setEnabled(false);
            bankNamejTextField.setEnabled(false);
            routingNumjTextField.setEnabled(false);
        }
        
    }//GEN-LAST:event_organizationEmpJComboBoxActionPerformed

    private void updatejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatejButtonActionPerformed
        // TODO add your handling code here:
        try{
        log.debug("Entering Hospital Admin ManageEmployeeJPanel updatejButtonActionPerformed");
       if (organizationEmpJComboBox.isEnabled()) 
       {
            JOptionPane.showMessageDialog(this, "Please select an employee to update details!");
            return;
       } 
       else 
       {
           
           
            String id = empNojTextField.getText();
            String errorMsg = "";
            Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
            if (firstnameJTextField.getText().isEmpty() && firstnameJTextField.getText().isEmpty() && empNojTextField.getText().isEmpty() && emailnameJTextField.getText().isEmpty() && phonejTextField.getText().isEmpty() && null == txtDob.getDate()) {
                JOptionPane.showMessageDialog(this, "Please enter values in all fields!");
                return;
            } else {
                if (firstnameJTextField.getText().isEmpty() || firstnameJTextField.getText().trim().length() == 0) {
                    errorMsg += "First-Name cannot be blank!";
                    firstnameJTextField.setText("");
                } else if (!validateLetters(firstnameJTextField.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "First-Name can contain only letters!" : errorMsg + "\nFirst-Name can contain only letters!";
                }

                if (lastjTextField.getText().isEmpty() || lastjTextField.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Last-Name cannot be blank!" : errorMsg + "\nLast-Name cannot be blank!";
                    lastjTextField.setText("");
                } else if (!validateLetters(lastjTextField.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Last-Name can contain only letters!" : errorMsg + "\nLast-Name can contain only letters!";
                }

                if (null == txtDob.getDate()) {
                    errorMsg = errorMsg.isEmpty() ? "Please select a Date-Of-Joining!" : errorMsg + "\nPlease select a Date-Of-Joining!";
                }

                if (phonejTextField.getText().isEmpty() || phonejTextField.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Mobile-Number cannot be blank!" : errorMsg + "\nMobile-Number cannot be blank!";
                    phonejTextField.setText("");
                } else if (!validatePhone(phonejTextField.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Mobile-Number can contain only digits!" : errorMsg + "\nMobile-Number can contain only digits!";
                }

                if (emailnameJTextField.getText().isEmpty() || emailnameJTextField.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Email cannot be blank!" : errorMsg + "\nEmail cannot be blank!";
                    phonejTextField.setText("");
                } else if (!validateEmail(emailnameJTextField.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Please enter a valid email!" : errorMsg + "\nPlease enter a valid email!";
                }
            }
            if (!errorMsg.isEmpty()) 
            {
                JOptionPane.showMessageDialog(null, errorMsg);
            } 
            else 
            {
                Employee emp = null;
                Organization empOrg = null;
                for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (Employee employee : org.getEmployeeDirectory().getEmployeeList()) {
                        if (employee.getEmpNo().equals(id)) {
                            emp = employee;
                            empOrg = org;
                            break;
                        }
                    }
                }

                if (null == emp || null == empOrg) {
                    JOptionPane.showMessageDialog(this, "No such employee exists!!");
                } 
                else 
                {
                    
                    if(organization.getName().equals("Doctor Organization"))
                    {
                        AccountDetails acc= new AccountDetails();
                        acc.setAccountName(bankNamejTextField.getText());
                        acc.setAccountNumber(bankNamejTextField.getText());
                        acc.setRoutingNumber(routingNumjTextField.getText());
                        emp.setAccountDetl(acc);
                        emp.setFirstName(firstnameJTextField.getText());
                        emp.setLastName(lastjTextField.getText());
                        emp.setDesignation(designationjTextField.getText());
                        emp.setSpecialization(specializationjTextField.getText());
                        emp.setDepartment((String) deptjComboBox.getSelectedItem());
                        emp.setEmail(emailnameJTextField.getText());
                        emp.setPhone(phonejTextField.getText());
                        emp.setDateofJoining(txtDob.getDate());
                        
                        
                    }
                    else
                    {
                        emp.setFirstName(firstnameJTextField.getText());
                        emp.setLastName(lastjTextField.getText());
                        emp.setDesignation(designationjTextField.getText());
                        emp.setSpecialization(specializationjTextField.getText());
                        emp.setDepartment((String) deptjComboBox.getSelectedItem());
                        emp.setEmail(emailnameJTextField.getText());
                        emp.setPhone(phonejTextField.getText());
                        emp.setDateofJoining(txtDob.getDate());
                    
                    }
                }
            }
         //  populateTable(organization); 
           
        } 
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
       log.debug("Exiting Hospital Admin ManageEmployeeJPanel updatejButtonActionPerformed");
       
    }//GEN-LAST:event_updatejButtonActionPerformed

    private void viewjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewjButtonActionPerformed
        // TODO add your handling code here:
        repopulate();
    }//GEN-LAST:event_viewjButtonActionPerformed

    private void canceljButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_canceljButtonActionPerformed
        // TODO add your handling code here:
        log.debug("Entering Hospital Admin ManageEmployeeJPanel canceljButtonActionPerformed");
        firstnameJTextField.setText("");
        lastjTextField.setText("");
        designationjTextField.setText("");
        empNojTextField.setText("");
        specializationjTextField.setText("");
        phonejTextField.setText("");
        emailnameJTextField.setText("");
        txtDob.setDate(null);
        bankNamejTextField.setText("");
        accNojTextField.setText("");
        routingNumjTextField.setText("");
        organizationEmpJComboBox.setEnabled(true);
        empNojTextField.setEnabled(true);
        log.debug("Exiting Hospital Admin ManageEmployeeJPanel canceljButtonActionPerformed");
    }//GEN-LAST:event_canceljButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accNojTextField;
    private javax.swing.JButton addJButton;
    private javax.swing.JButton backJButton;
    private javax.swing.JTextField bankNamejTextField;
    private javax.swing.JButton canceljButton;
    private javax.swing.JComboBox deptjComboBox;
    private javax.swing.JTextField designationjTextField;
    private javax.swing.JTextField emailnameJTextField;
    private javax.swing.JTextField empNojTextField;
    private javax.swing.JTextField firstnameJTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField lastjTextField;
    private javax.swing.JComboBox organizationEmpJComboBox;
    private javax.swing.JComboBox organizationJComboBox;
    private javax.swing.JTable organizationJTable;
    private javax.swing.JTextField phonejTextField;
    private javax.swing.JTextField routingNumjTextField;
    private javax.swing.JTextField specializationjTextField;
    private com.toedter.calendar.JDateChooser txtDob;
    private javax.swing.JButton updatejButton;
    private javax.swing.JLabel userjLabel;
    private javax.swing.JButton viewjButton;
    // End of variables declaration//GEN-END:variables
}
    
