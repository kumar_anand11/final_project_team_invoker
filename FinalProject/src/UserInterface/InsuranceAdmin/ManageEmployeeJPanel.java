/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.InsuranceAdmin;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import UserInterface.DoctorRole.DoctorWorkAreaJPanel;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author putka
 */
public class ManageEmployeeJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageEmployeeJPanel
     */
    private Logger log=Logger.getLogger(ManageEmployeeJPanel.class);
    private OrganizationDirectory organizationDir;
    private JPanel userProcessContainer;
    private EcoSystem ecosystem;
    private Enterprise enterprise;
    private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    
    public ManageEmployeeJPanel(JPanel userProcessContainer,OrganizationDirectory organizationDir, Enterprise enterprise,EcoSystem ecosystem) {
        log.debug("Entering ManageEmployeeJPanel InsuranceAdmin constrcutor");
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organizationDir = organizationDir;
        this.ecosystem=ecosystem;
        this.enterprise=enterprise;
        
        populateOrganizationComboBox();
        log.debug("Exiting ManageEmployeeJPanel InsuranceAdmin constrcutor");
 //       populateOrganizationEmpComboBox();
    }
    
    private void populateTable(Organization organization){
        try{
        log.debug("Entering ManageEmployeeJPanel InsuranceAdmin populateTable");
        DefaultTableModel model = (DefaultTableModel) tblOrganization.getModel();
        
        model.setRowCount(0);
        
        for (Employee employee : organization.getEmployeeDirectory().getEmployeeList()) {
            Object[] row = new Object[3];
            row[0] = employee.getEmpNo();
            row[1] = employee.getLastName()+", "+employee.getFirstName();
            row[2] = sf.format(employee.getDateofJoining());
            model.addRow(row);
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting ManageEmployeeJPanel InsuranceAdmin populateTable");
    }
    
    public void populateOrganizationComboBox()
    {
        organizationJComboBox.removeAllItems();
        organizationEmpJComboBox.removeAllItems();
        
        for (Organization organization : organizationDir.getOrganizationList())
        {
            organizationJComboBox.addItem(organization);
            organizationEmpJComboBox.addItem(organization);
            
        }
    }
    
    public void populateOrganizationEmpComboBox()
    {
        organizationEmpJComboBox.removeAllItems();
        
        for (Organization organization : organizationDir.getOrganizationList())
        {
            organizationEmpJComboBox.addItem(organization);
        }
    }
    
    public boolean validateLetters(String txt) {
        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validatePhone(String txt) {
        String regx = "\\d{10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateEmpNo(String txt) {
        String regx = "\\d{1,10}";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateEmail(String txt) {
        String regx = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public boolean validateLettersAndDigits(String txt) {
        String regx = "^[a-zA-Z0-9\\s.,-]*$";
        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();
    }
    
    public long calcDateDiff(Date d1){
        long days=0;
        LocalDate currentDate = LocalDate.now();
        LocalDate doj = d1.toInstant().atZone(ZoneId.systemDefault().systemDefault().systemDefault()).toLocalDate();
        days=ChronoUnit.DAYS.between(doj, currentDate);
        return days;
    }
    
    public boolean checkExployeeExists(String empNo) {
        boolean isExists = false;
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
            for (Employee employee : org.getEmployeeDirectory().getEmployeeList()) {
                if (employee.getEmpNo().equals(empNo)) {
                    isExists = true;
                    break;
                }
            }
        }
        return isExists;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        organizationJComboBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrganization = new javax.swing.JTable();
        backJButton = new javax.swing.JButton();
        btnCreateEmp = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        organizationEmpJComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        emailJTextField1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        phonejTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtEmpNo = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        hospitaljComboBox = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        lblLastName = new javax.swing.JLabel();
        txtLastName = new javax.swing.JTextField();
        txtDateJoining = new com.toedter.calendar.JDateChooser();
        btnCancel = new javax.swing.JButton();
        btnUpdateEmp = new javax.swing.JButton();
        btnView = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 153, 153));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel7.setText("ADD EMPLOYEES TO ORGANIZATION");

        jLabel1.setText("Organization-Filter:");

        organizationJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationJComboBoxActionPerformed(evt);
            }
        });

        tblOrganization.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Employee-Number", "Employee-Name", "Date of Joining"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblOrganization);

        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        btnCreateEmp.setText("Create Employee");
        btnCreateEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateEmpActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Employee-Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel3.setText("Organization :");

        organizationEmpJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        organizationEmpJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationEmpJComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("First Name :");

        jLabel4.setText("Email :");

        jLabel6.setText("Date Of Joining :");

        jLabel8.setText("Phone :");

        jLabel10.setText("Employee-Number:");

        jLabel9.setText("Hospital Name :");

        hospitaljComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/insuranceRole.jpg"))); // NOI18N

        lblLastName.setText("Last Name :");

        txtDateJoining.setDateFormatString("MM/dd/yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel10)
                                    .addComponent(lblLastName))
                                .addGap(52, 52, 52)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtEmpNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                                    .addComponent(txtFirstName, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtLastName, javax.swing.GroupLayout.Alignment.LEADING)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                                .addComponent(organizationEmpJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6))
                        .addGap(67, 67, 67)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(phonejTextField)
                            .addComponent(emailJTextField1)
                            .addComponent(hospitaljComboBox, 0, 202, Short.MAX_VALUE)
                            .addComponent(txtDateJoining, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 152, Short.MAX_VALUE)))
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(organizationEmpJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblLastName)
                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtEmpNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(txtDateJoining, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(phonejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(emailJTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(hospitaljComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(77, 77, 77))))
        );

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnUpdateEmp.setText("Update Employee");
        btnUpdateEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateEmpActionPerformed(evt);
            }
        });

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backJButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCreateEmp)
                                .addGap(18, 18, 18)
                                .addComponent(btnUpdateEmp)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancel))
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnView)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 874, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(51, 51, 51)
                                        .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 695, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(221, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnView)
                .addGap(45, 45, 45)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backJButton)
                    .addComponent(btnCreateEmp)
                    .addComponent(btnCancel)
                    .addComponent(btnUpdateEmp))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void organizationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationJComboBoxActionPerformed
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        if (organization != null){
            populateTable(organization);
        }
    }//GEN-LAST:event_organizationJComboBoxActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void btnCreateEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateEmpActionPerformed
        
        try{
            log.debug("Entering ManageEmployeeJPanel InsuranceAdmin btnCreateEmpActionPerformed");
            String errorMsg = "";
        
        Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
        if (txtFirstName.getText().isEmpty() && txtLastName.getText().isEmpty() && txtEmpNo.getText().isEmpty() && emailJTextField1.getText().isEmpty() && phonejTextField.getText().isEmpty() && null == txtDateJoining.getDate()) {
            JOptionPane.showMessageDialog(this, "Please enter values in all fields!");
            return;
        } else {
            if (txtFirstName.getText().isEmpty() || txtFirstName.getText().trim().length() == 0) {
                errorMsg += "First-Name cannot be blank!";
                txtFirstName.setText("");
            } else if (!validateLetters(txtFirstName.getText())) {
                errorMsg = errorMsg.isEmpty() ? "First-Name can contain only letters!" : errorMsg + "\nFirst-Name can contain only letters!";
            }

            if (txtLastName.getText().isEmpty() || txtLastName.getText().trim().length() == 0) {
                errorMsg = errorMsg.isEmpty() ? "Last-Name cannot be blank!" : errorMsg + "\nLast-Name cannot be blank!";
                txtLastName.setText("");
            } else if (!validateLetters(txtLastName.getText())) {
                errorMsg = errorMsg.isEmpty() ? "Last-Name can contain only letters!" : errorMsg + "\nLast-Name can contain only letters!";
            }

            try {
                if (txtEmpNo.getText().isEmpty() || txtEmpNo.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Employee-Number cannot be blank!" : errorMsg + "\nEmployee-Number cannot be blank!";
                    txtEmpNo.setText("");
                } else if (!validateEmpNo(txtEmpNo.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Employee-Number can contain only digits!" : errorMsg + "\nEmployee-Number can contain only digits!";
                } else if (0 == Integer.parseInt(txtEmpNo.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Employee-Number cannot be 0!" : errorMsg + "\nEmployee-Number cannot be 0!";
                    txtEmpNo.setText("");
                } else if (checkExployeeExists(txtEmpNo.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Employee with the give Employee-Number already exists!" : errorMsg + "\nEmployee with the give Employee-Number already exists!";
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
                errorMsg = errorMsg.isEmpty() ? "Employee-Number can contain only digits!" : errorMsg + "\nEmployee-Number can contain only digits!";
            }

            if (null == txtDateJoining.getDate()) {
                errorMsg = errorMsg.isEmpty() ? "Please select a Date-Of-Joining!" : errorMsg + "\nPlease select a Date-Of-Joining!";
            } else if (calcDateDiff(txtDateJoining.getDate()) <= 0) {
                errorMsg = errorMsg.isEmpty() ? "Date-Of-Joining cannot be a future date" : errorMsg + "\nDate-Of-Joining cannot be a future date!";
            }

            if (phonejTextField.getText().isEmpty() || phonejTextField.getText().trim().length() == 0) {
                errorMsg = errorMsg.isEmpty() ? "Mobile-Number cannot be blank!" : errorMsg + "\nMobile-Number cannot be blank!";
                phonejTextField.setText("");
            } else if (phonejTextField.getText().length() < 10) {
                errorMsg = errorMsg.isEmpty() ? "Mobile-Number should be of 10 digits!" : errorMsg + "\nMobile-Number should be of 10 digits!";
            } else if (!validatePhone(phonejTextField.getText())) {
                errorMsg = errorMsg.isEmpty() ? "Mobile-Number can contain only digits!" : errorMsg + "\nMobile-Number can contain only digits!";
            }

            if (emailJTextField1.getText().isEmpty() || emailJTextField1.getText().trim().length() == 0) {
                errorMsg = errorMsg.isEmpty() ? "Email cannot be blank!" : errorMsg + "\nEmail cannot be blank!";
                phonejTextField.setText("");
            } else if (!validateEmail(emailJTextField1.getText())) {
                errorMsg = errorMsg.isEmpty() ? "Please enter a valid email!" : errorMsg + "\nPlease enter a valid email!";
            }
        }
        if (!errorMsg.isEmpty()) {
            JOptionPane.showMessageDialog(null, errorMsg);
        } else {
            int dialogRes = JOptionPane.showConfirmDialog(this, "Are you sure you want to create the employee?");
            if (dialogRes == JOptionPane.YES_OPTION) {
                Employee emp = organization.getEmployeeDirectory().createEmployee(txtFirstName.getText(), txtLastName.getText(), txtEmpNo.getText(), null, null, emailJTextField1.getText(), phonejTextField.getText(), txtDateJoining.getDate(), null, null);

                if (hospitaljComboBox.isEnabled()) {
                    emp.setHospitalAssignment(hospitaljComboBox.getSelectedItem().toString());
                }
                JOptionPane.showMessageDialog(null, "Employee created successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
                txtFirstName.setText("");
                txtLastName.setText("");
                txtEmpNo.setText("");
                emailJTextField1.setText("");
                phonejTextField.setText("");
                txtDateJoining.setDate(null);
                populateOrganizationEmpComboBox();
                organizationEmpJComboBox.setEnabled(true);
                populateTable(organization);
            }
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting ManageEmployeeJPanel InsuranceAdmin btnCreateEmpActionPerformed");
    }//GEN-LAST:event_btnCreateEmpActionPerformed

    private void organizationEmpJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationEmpJComboBoxActionPerformed
        // TODO add your handling code here:
        
        hospitaljComboBox.removeAllItems();
        Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
        if (organization== null)
        {
            hospitaljComboBox.setEnabled(false);
        }
        else if(organization.getName().equals("Auditor Organization"))
        {
            hospitaljComboBox.setEnabled(true);
            for(Network network : ecosystem.getNetworkList())
            {
                for(Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList())
                {
                    if(enterprise.getEnterpriseType().getValue().equalsIgnoreCase("hospital")){
                        hospitaljComboBox.addItem(enterprise.getName());
                    }
                }
                
            }
        }
        else
        {
            hospitaljComboBox.setEnabled(false);
        }
    }//GEN-LAST:event_organizationEmpJComboBoxActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        txtFirstName.setText("");
        txtLastName.setText("");
        txtEmpNo.setText("");
        emailJTextField1.setText("");
        phonejTextField.setText("");
        txtDateJoining.setDate(null);
        populateOrganizationEmpComboBox();
        organizationEmpJComboBox.setEnabled(true);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnUpdateEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateEmpActionPerformed
        // TODO add your handling code here:
        try{
        log.debug("Entering ManageEmployeeJPanel InsuranceAdmin btnUpdateEmpActionPerformed");
        if (organizationEmpJComboBox.isEnabled()) {
            JOptionPane.showMessageDialog(this, "Please select an employee to update details!");
        } else {
            String id = txtEmpNo.getText();
            String errorMsg = "";
            Organization organization = (Organization) organizationEmpJComboBox.getSelectedItem();
            if (txtFirstName.getText().isEmpty() && txtLastName.getText().isEmpty() && txtEmpNo.getText().isEmpty() && emailJTextField1.getText().isEmpty() && phonejTextField.getText().isEmpty() && null == txtDateJoining.getDate()) {
                JOptionPane.showMessageDialog(this, "Please enter values in all fields!");
                return;
            } else {
                if (txtFirstName.getText().isEmpty() || txtFirstName.getText().trim().length() == 0) {
                    errorMsg += "First-Name cannot be blank!";
                    txtFirstName.setText("");
                } else if (!validateLetters(txtFirstName.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "First-Name can contain only letters!" : errorMsg + "\nFirst-Name can contain only letters!";
                }

                if (txtLastName.getText().isEmpty() || txtLastName.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Last-Name cannot be blank!" : errorMsg + "\nLast-Name cannot be blank!";
                    txtLastName.setText("");
                } else if (!validateLetters(txtLastName.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Last-Name can contain only letters!" : errorMsg + "\nLast-Name can contain only letters!";
                }

                if (null == txtDateJoining.getDate()) {
                    errorMsg = errorMsg.isEmpty() ? "Please select a Date-Of-Joining!" : errorMsg + "\nPlease select a Date-Of-Joining!";
                }

                if (phonejTextField.getText().isEmpty() || phonejTextField.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Mobile-Number cannot be blank!" : errorMsg + "\nMobile-Number cannot be blank!";
                    phonejTextField.setText("");
                } else if (!validatePhone(phonejTextField.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Mobile-Number can contain only digits!" : errorMsg + "\nMobile-Number can contain only digits!";
                }

                if (emailJTextField1.getText().isEmpty() || emailJTextField1.getText().trim().length() == 0) {
                    errorMsg = errorMsg.isEmpty() ? "Email cannot be blank!" : errorMsg + "\nEmail cannot be blank!";
                    phonejTextField.setText("");
                } else if (!validateEmail(emailJTextField1.getText())) {
                    errorMsg = errorMsg.isEmpty() ? "Please enter a valid email!" : errorMsg + "\nPlease enter a valid email!";
                }
            }
            if (!errorMsg.isEmpty()) {
                JOptionPane.showMessageDialog(null, errorMsg);
            } else {
                Employee emp = null;
                Organization empOrg = null;
                for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (Employee employee : org.getEmployeeDirectory().getEmployeeList()) {
                        if (employee.getEmpNo().equals(id)) {
                            emp = employee;
                            empOrg = org;
                            break;
                        }
                    }
                }

                if (null == emp || null == empOrg) {
                    JOptionPane.showMessageDialog(this, "No such employee exists!!");
                } else {
                    emp.setFirstName(txtFirstName.getText());
                    emp.setLastName(txtLastName.getText());
                    emp.setEmail(emailJTextField1.getText());
                    emp.setPhone(phonejTextField.getText());
                    emp.setDateofJoining(txtDateJoining.getDate());
                }
                if (hospitaljComboBox.isEnabled()) {
                    emp.setHospitalAssignment(hospitaljComboBox.getSelectedItem().toString());
                    System.out.println("Hosp:" + hospitaljComboBox.getSelectedItem().toString());
                }
                populateTable(organization);

            }
        }
        }
        catch(Exception e){
            log.error(e.getMessage());
        }
        log.debug("Exiting ManageEmployeeJPanel InsuranceAdmin btnUpdateEmpActionPerformed");

    }//GEN-LAST:event_btnUpdateEmpActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
        try{
         log.debug("Entering ManageEmployeeJPanel InsuranceAdmin btnViewActionPerformed");   
        hospitaljComboBox.setEnabled(false);
        int selectedRow = tblOrganization.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please select an employee!!");
            return;
        }
        //Get request from org work-queue
        String id = (String) tblOrganization.getValueAt(selectedRow, 0);
        Employee emp = null;
        Organization empOrg = null;
        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
            for (Employee employee : org.getEmployeeDirectory().getEmployeeList()) {
                if (employee.getEmpNo().equals(id)) {
                    emp = employee;
                    empOrg = org;
                    break;
                }
            }
        }

        if (null == emp || null == empOrg) {
            JOptionPane.showMessageDialog(this, "No such employee exists!!");
        } else {
            txtFirstName.setText(emp.getFirstName());
            txtLastName.setText(emp.getLastName());
            txtEmpNo.setText(emp.getEmpNo());
            txtEmpNo.setEnabled(false);
            emailJTextField1.setText(emp.getEmail());
            phonejTextField.setText(emp.getPhone());
            txtDateJoining.setDate(emp.getDateofJoining());
            organizationEmpJComboBox.setSelectedItem(empOrg);
            System.out.println(empOrg.getName());
            organizationEmpJComboBox.setEnabled(false);
            if ("Auditor Organization".equals(empOrg.getName())) {
                hospitaljComboBox.setEnabled(true);
                for (Network network : ecosystem.getNetworkList()) {
                    for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                        if (enterprise.getEnterpriseType().getValue().equalsIgnoreCase("hospital")) {
                            hospitaljComboBox.addItem(enterprise.getName());
                        }
                    }

                }
                hospitaljComboBox.setSelectedItem(emp.getHospitalAssignment());
              }
            
        }
    }
        catch(Exception e){
            log.error(e.getMessage());
        }
        log.debug("Exiting ManageEmployeeJPanel InsuranceAdmin btnViewActionPerformed");   
    }//GEN-LAST:event_btnViewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCreateEmp;
    private javax.swing.JButton btnUpdateEmp;
    private javax.swing.JButton btnView;
    private javax.swing.JTextField emailJTextField1;
    private javax.swing.JComboBox hospitaljComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JComboBox organizationEmpJComboBox;
    private javax.swing.JComboBox organizationJComboBox;
    private javax.swing.JTextField phonejTextField;
    private javax.swing.JTable tblOrganization;
    private com.toedter.calendar.JDateChooser txtDateJoining;
    private javax.swing.JTextField txtEmpNo;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    // End of variables declaration//GEN-END:variables
}
