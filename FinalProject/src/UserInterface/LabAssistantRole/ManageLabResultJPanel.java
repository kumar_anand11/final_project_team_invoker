/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.LabAssistantRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.LabAssistantOrganization;
import Business.Organization.*;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.HospitalWorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

/**
 *
 * @author putka
 */
public class ManageLabResultJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageLabResultJPanel
     */
    private Logger log=Logger.getLogger(ManageLabResultJPanel.class);
    private JPanel userContainerProcess;
    private LabAssistantOrganization laborg;
    private Enterprise enterprise;
    private EcoSystem ecosystem;
    private UserAccount userAccount;
    private HospitalWorkRequest request;
    private String filepath;
    
    public ManageLabResultJPanel(JPanel userContainerProcess, UserAccount userAccount, LabAssistantOrganization laborg, Enterprise enterprise, EcoSystem ecosystem,HospitalWorkRequest request) {
        log.debug("Entering ManageLabResultJPanel constructor");
        initComponents();
        this.userContainerProcess=userContainerProcess;
        this.laborg=laborg;
        this.enterprise=enterprise;
        this.ecosystem=ecosystem;
        this.userAccount=userAccount;
        this.request=request;
        repopulate();
       log.debug("Exitting ManageLabResultJPanel constructor");
    }

    public String getDocName(String docId)
    {
        String name= null;
        for (Network network : ecosystem.getNetworkList()) {
            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("hospital")) {
                    for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (organization instanceof DoctorOrganization) {
                            for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                                if (ua.getUsername().equals(docId)) {
                              //      System.out.println("Doctor-Name :" + ua.getEmployee().getFirstName()+", "+ua.getEmployee().getLastName());
                                    name = ua.getEmployee().getFirstName()+", "+ua.getEmployee().getLastName();
                                    break;
                                }
                            }
                        }
                    }

                }
            }
        }
        return name;
    }
    public void repopulate()
    {  
        try{
        log.debug("Entering ManageLabResultJPanel populate");
        visitIdjTextField.setText(request.getVisitID());
        patientIdjTextField.setText(request.getPatient().getPatientID());
        namejTextField.setText(request.getPatient().getFirstName()+" "+request.getPatient().getLastName());
        doctorjTextField.setText(getDocName(request.getDoctorID()));
        labTypejTextField.setText(request.getMedication().getLabtype());  //have to change this to lab type
        
        //repopulating billing information
        
        labTypeinfojTextField.setText(request.getMedication().getLabtype());  //have to change this to lab type
        donebyjTextField.setText(request.getLabID());
        
        remarksjTextField.setText(request.getLabResult());
        
        visitIdjTextField.setEditable(false);
        patientIdjTextField.setEditable(false);
        namejTextField.setEditable(false);
        doctorjTextField.setEditable(false);
        labTypejTextField.setEditable(false);
        labTypeinfojTextField.setEditable(false);
        donebyjTextField.setEditable(false);
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exitting ManageLabResultJPanel populate");
        
    }
    
    public long calcDateDiff(Date d1){
        long days=0;
        LocalDate currentDate = LocalDate.now();
        LocalDate dateofResult = d1.toInstant().atZone(ZoneId.systemDefault().systemDefault().systemDefault()).toLocalDate();
        days=ChronoUnit.DAYS.between(dateofResult, currentDate);
        return days;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        backjButton = new javax.swing.JButton();
        savejButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        patientIdjTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        namejTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        visitIdjTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        doctorjTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        labTypeinfojTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        resultjTextField = new javax.swing.JTextField();
        uploadjButton = new javax.swing.JButton();
        imageLabel = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        remarksjTextField = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labTypejTextField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        donebyjTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtDob = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        labChargesjTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("LAB RESULTS");

        backjButton.setText("<<Back");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        savejButton.setText("Save");
        savejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savejButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "PATIENT DETAILS", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 13))); // NOI18N

        jLabel2.setText("Patient Id :");

        jLabel3.setText("Patient Name :");

        jLabel10.setText("Visit Id :");

        visitIdjTextField.setEditable(false);

        jLabel4.setText("Doctor :");

        jLabel6.setText("Lab Type :");

        jLabel5.setText("Report :*");

        uploadjButton.setText("Upload");
        uploadjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadjButtonActionPerformed(evt);
            }
        });

        imageLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel13.setText("Remarks : ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addGap(33, 33, 33))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(doctorjTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                        .addComponent(labTypeinfojTextField)
                        .addComponent(visitIdjTextField, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(remarksjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(155, 155, 155)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(namejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(resultjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(uploadjButton))
                            .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(patientIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(87, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(doctorjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(namejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel10)
                            .addComponent(visitIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(patientIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(labTypeinfojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(resultjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(uploadjButton)
                    .addComponent(jLabel5))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(remarksjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(imageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(28, 43, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LAB BILLING INFORMATION", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 13))); // NOI18N

        jLabel7.setText("Lab Type :");

        labTypejTextField.setEditable(false);

        jLabel8.setText("Done By :");

        donebyjTextField.setEditable(false);

        jLabel9.setText("Date :*");

        txtDob.setDateFormatString("MM/dd/yyyy");

        jLabel11.setText("Lab Charges :*");

        jLabel12.setText("$");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7))
                .addGap(47, 47, 47)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labTypejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDob, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(donebyjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(87, 87, 87)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labChargesjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(labTypejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(donebyjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)))
                .addGap(3, 3, 3)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel9))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labChargesjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(jLabel12)))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(461, 461, 461)
                .addComponent(backjButton)
                .addGap(138, 138, 138)
                .addComponent(savejButton)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel1))
                .addGap(540, 655, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savejButton)
                    .addComponent(backjButton))
                .addContainerGap(82, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void savejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savejButtonActionPerformed
        // TODO add your handling code here:
        try{
        log.debug("Entering ManageLabResultJPanel savejButtonActionPerformed");
        if(resultjTextField.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Reports cannot be empty");
            return;
        }
        if(labChargesjTextField.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Charges not added!");
            return;
        }
        try
        {
            Double.parseDouble(labChargesjTextField.getText());
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Charges cannot be characters");
            return;
        }
 
        if(txtDob.getDate()==null)
        {
            JOptionPane.showMessageDialog(null, "Date  cannot be empty!");
            return;
        }
        else if(calcDateDiff(txtDob.getDate())<=0)
        {
            JOptionPane.showMessageDialog(null, "Date-Of-Result cannot be a future date!");
            return;
            
        }
        
        
        request.setLabFees(Double.valueOf(labChargesjTextField.getText()));
        request.setDate(txtDob.getDate());
        
        
        
        request.setStatus("Lab Results Available");
        request.setLabStatus("Completed");
        
        if(request.getLabStatus().equals("Completed"))
        {
            Organization docAssOrg = null;
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList())
            {
                if(organization instanceof DoctorAssistantOrganization)
                {
                    docAssOrg=organization;
                    break;
                }
            }
            if(docAssOrg!=null)
            {
                docAssOrg.getWorkQueue().getWorkRequestList().add(request);
                laborg.getWorkQueue().getWorkRequestList().remove(request);
                userAccount.getWorkQueue().getWorkRequestList().remove(request);
            }
        }
        
        JOptionPane.showMessageDialog(this, "Lab Result added Successfully!");
        
        savejButton.setEnabled(false);
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ManageLabResultJPanel savejButtonActionPerformed");
    }//GEN-LAST:event_savejButtonActionPerformed

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
        // TODO add your handling code here:
        userContainerProcess.remove(this);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.previous(userContainerProcess);
    }//GEN-LAST:event_backjButtonActionPerformed

    private void uploadjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadjButtonActionPerformed
        // TODO add your handling code here:
        // TODO add your handling code here:
        try{
        log.debug("Entering ManageLabResultJPanel uploadjButtonActionPerformed");
        try
        {
            JFileChooser fileChooser= new JFileChooser();
            fileChooser.showDialog(jPanel1, null);
            File file= fileChooser.getSelectedFile();
            resultjTextField.setText(file.getAbsolutePath());
            BufferedImage buffImg = ImageIO.read(new File(file.getAbsolutePath()));
            Image scaledImg = buffImg.getScaledInstance(imageLabel.getWidth(), imageLabel.getHeight(),Image.SCALE_SMOOTH);
            scaledImg.getScaledInstance(179, 237, Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(scaledImg);
            imageLabel.setIcon(imageIcon);
            JOptionPane.showMessageDialog(null, "Photo uploaded successfully"); 
        }
        catch(Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "An error occurred during uploading of photo. Please try again.");
        }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
            
        }
        log.debug("Exiting ManageLabResultJPanel uploadjButtonActionPerformed");
    }//GEN-LAST:event_uploadjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JTextField doctorjTextField;
    private javax.swing.JTextField donebyjTextField;
    private javax.swing.JLabel imageLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField labChargesjTextField;
    private javax.swing.JTextField labTypeinfojTextField;
    private javax.swing.JTextField labTypejTextField;
    private javax.swing.JTextField namejTextField;
    private javax.swing.JTextField patientIdjTextField;
    private javax.swing.JTextField remarksjTextField;
    private javax.swing.JTextField resultjTextField;
    private javax.swing.JButton savejButton;
    private com.toedter.calendar.JDateChooser txtDob;
    private javax.swing.JButton uploadjButton;
    private javax.swing.JTextField visitIdjTextField;
    // End of variables declaration//GEN-END:variables
}
