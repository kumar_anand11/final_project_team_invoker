/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Supervisor;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.AgentOrganization;
import Business.Organization.AuditorOrganization;
import Business.Organization.Organization;
import Business.Organization.SupervisorOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.InsuranceWorkRequest;
import Business.WorkQueue.Remark;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

/**
 *
 * @author putka
 */
public class ProcessClaimJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ProcessClaimJPanel
     */
    private Logger log=Logger.getLogger(ProcessClaimJPanel.class);
private JPanel userProcessContainer;
private SupervisorOrganization organization;
private Enterprise enterprise;
private EcoSystem ecosystem;
private UserAccount userAccount;
private InsuranceWorkRequest request;
private SimpleDateFormat sf=new SimpleDateFormat("dd-MMM-yyyy");
    public ProcessClaimJPanel(JPanel userProcessContainer, UserAccount account, SupervisorOrganization organization, Enterprise enterprise,EcoSystem ecosystem, InsuranceWorkRequest request) {
        log.debug("Entering ProcessClaimJPanel Supervisor constructor");
        initComponents();       
        this.userProcessContainer=userProcessContainer;
        this.ecosystem=ecosystem;
        this.enterprise=enterprise;
        this.organization=organization;
        this.userAccount=account;
        this.request=request;   
        populate();
        log.debug("Exiting ProcessClaimJPanel Supervisor constructor");
    }
    public void populate(){ 
        try{
            
        log.debug("Entering ProcessClaimJPanel Supervisor popdata");
        txtPolicyID.setText(request.getHospitalReq().getPatient().getPolicy().getPolicyID());
        txtStartDate.setText(sf.format(request.getHospitalReq().getPatient().getPolicy().getStartDate()));
        txtEndDate.setText(sf.format(request.getHospitalReq().getPatient().getPolicy().getEndDate()));
        txtGrade.setText(request.getHospitalReq().getPatient().getPolicy().getPolicyGrade());
        
        Date policyDate = request.getPolicy().getEndDate();
        Date currDate = new Date();
        if (currDate.after(policyDate)){
            policyStatusTextField.setText("Expired");
        }
        else
        {
            policyStatusTextField.setText("Active");
        }
        txtPolHoldName.setText((request.getHospitalReq().getPatient().getLastName() + ", " + request.getHospitalReq().getPatient().getFirstName()));
        long diff = ChronoUnit.DAYS.between(request.getRequestDate().toInstant(), new Date().toInstant());
        if (diff > 1) 
        {
          agingTextField.setText(diff + "Days")   ;
        }
        if (diff <= 1)
        {
         agingTextField.setText("0 Days");
         }
        claimIDTextField.setText(request.getClaimID());
        hosptalIDTextField.setText(request.getHospitalReq().getHospitalID());
        claiDateTextField.setText(sf.format(request.getRequestDate()));        
        claimAmntTextField.setText(String.valueOf(request.getClaimAmnt()));        
        claimSatusTextField.setText(request.getStatus());
        agentIDTextField.setText(request.getAgentID());
        for (Remark rem : request.getRemarkList()) {
         if (rem.getRole().contentEquals("Agent")){
            if (agentRemarksTextField.getText().isEmpty())
            {
             agentRemarksTextField.setText(rem.getComment());
            }
            else
            {
                agentRemarksTextField.setText(agentRemarksTextField.getText() + ",\n" +rem.getComment() );
            }
         }        
    }
 //////////////Comment History
        DefaultTableModel dtm = (DefaultTableModel) tblCommHist.getModel(); 
        dtm = (DefaultTableModel) tblCommHist.getModel();
        dtm.setRowCount(0);
        boolean flagComment = false;
        Remark delRemark = null;
        for (Remark rem : request.getRemarkList()) {
            if (null != rem.getStatus() && rem.getStatus().equalsIgnoreCase("save")) {
                flagComment = true;
                delRemark = rem;
            } else {
                Object[] row = new Object[4];
                row[0] = sf.format(rem.getCreationDate());
                row[1] = rem.getUsername();
                row[2] = rem.getRole();
                row[3] = rem.getComment();

                dtm.addRow(row);
            }
        }
        
        if(flagComment){
           supervisorRemrks.setText(delRemark.getComment());
           request.getRemarkList().remove(delRemark);
        }else{
            supervisorRemrks.setText("");
        }
        }
        catch(Exception e)
        {
           log.error(e.getMessage());
                
        }
        log.debug("Exiting ProcessClaimJPanel Supervisor popdata");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        agentIDTextField = new javax.swing.JTextField();
        agingTextField = new javax.swing.JTextField();
        claimAmntTextField = new javax.swing.JTextField();
        claimIDTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        hosptalIDTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        claiDateTextField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        claimSatusTextField = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        agentRemarksTextField = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        svBnkNamLbl = new javax.swing.JLabel();
        txtPolicyID = new javax.swing.JTextField();
        svBnkRoutngNumLbl1 = new javax.swing.JLabel();
        svBnkNamLbl4 = new javax.swing.JLabel();
        txtPolHoldName = new javax.swing.JTextField();
        svBnkAccBalLbl = new javax.swing.JLabel();
        txtGrade = new javax.swing.JTextField();
        svBnkRoutngNumLbl = new javax.swing.JLabel();
        txtStartDate = new javax.swing.JTextField();
        txtEndDate = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        policyStatusTextField = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblCommHist = new javax.swing.JTable();
        sendBackbtn = new javax.swing.JButton();
        sendAuditor = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        saveBtn = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        supervisorRemrks = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 153, 153));

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Claim Details:");
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Claim ID:");
        jLabel9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Claim Amount:");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Pending Since:");
        jLabel18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Pass By Agent:");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        agentIDTextField.setEditable(false);

        agingTextField.setEditable(false);

        claimAmntTextField.setEditable(false);

        claimIDTextField.setEditable(false);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Hospital ID:");
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        hosptalIDTextField.setEditable(false);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Claim Date:");
        jLabel13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        claiDateTextField.setEditable(false);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Claim Status:");
        jLabel14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        claimSatusTextField.setEditable(false);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Agent Remarks:");
        jLabel15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        agentRemarksTextField.setEditable(false);
        agentRemarksTextField.setColumns(20);
        agentRemarksTextField.setRows(5);
        jScrollPane2.setViewportView(agentRemarksTextField);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                                .addComponent(agentIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(claimAmntTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(claimIDTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(agingTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(26, 26, 26)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel13)
                                            .addGap(52, 52, 52))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel10)
                                            .addGap(51, 51, 51)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel14)
                                        .addGap(43, 43, 43)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(claiDateTextField)
                                    .addComponent(hosptalIDTextField)
                                    .addComponent(claimSatusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jLabel8))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(claimIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(hosptalIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(claimAmntTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(claiDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(agingTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(claimSatusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(agentIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Policy Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel3.setMinimumSize(new java.awt.Dimension(50, 50));

        svBnkNamLbl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        svBnkNamLbl.setText("Policy-ID:");

        txtPolicyID.setEditable(false);

        svBnkRoutngNumLbl1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        svBnkRoutngNumLbl1.setText("Start-Date:");

        svBnkNamLbl4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        svBnkNamLbl4.setText("Policy-Holder's Name:");

        txtPolHoldName.setEditable(false);

        svBnkAccBalLbl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        svBnkAccBalLbl.setText("Policy-Grade:");

        txtGrade.setEditable(false);

        svBnkRoutngNumLbl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        svBnkRoutngNumLbl.setText("End-Date:");

        txtStartDate.setEditable(false);

        txtEndDate.setEditable(false);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("Policy Satus:");
        jLabel26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        policyStatusTextField.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl)
                    .addComponent(svBnkRoutngNumLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolicyID, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(txtStartDate))
                .addGap(53, 53, 53)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkNamLbl4)
                    .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPolHoldName)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(svBnkAccBalLbl)
                    .addComponent(jLabel26))
                .addGap(35, 35, 35)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtGrade, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(policyStatusTextField))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(svBnkNamLbl)
                    .addComponent(txtPolicyID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(svBnkNamLbl4)
                    .addComponent(txtPolHoldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(svBnkAccBalLbl)
                    .addComponent(txtGrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel26)
                        .addComponent(policyStatusTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(svBnkRoutngNumLbl1)
                        .addComponent(svBnkRoutngNumLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(0, 153, 153));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Communication History", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel10.setMinimumSize(new java.awt.Dimension(50, 50));

        tblCommHist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Date", "Name", "Role", "Comments"
            }
        ));
        jScrollPane3.setViewportView(tblCommHist);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        sendBackbtn.setText("Send Back To Agent");
        sendBackbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendBackbtnActionPerformed(evt);
            }
        });

        sendAuditor.setText("Auditor Clearance");
        sendAuditor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendAuditorActionPerformed(evt);
            }
        });

        btnBack.setText("<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        saveBtn.setText("Save Remarks");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        supervisorRemrks.setColumns(20);
        supervisorRemrks.setRows(5);
        jScrollPane4.setViewportView(supervisorRemrks);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Supervisor Remarks:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 727, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Claim Process");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE)
                                    .addComponent(sendBackbtn)
                                    .addGap(118, 118, 118)
                                    .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(102, 102, 102)
                                    .addComponent(sendAuditor))
                                .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(87, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sendBackbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sendAuditor, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void sendAuditorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendAuditorActionPerformed
        // TODO add your handling code here:
            //Add this Work-Request to queue
        try{
            
        log.debug("Entering ProcessClaimJPanel Supervisor sendAuditorActionPerformed");
        if (supervisorRemrks.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Supervisor Remarks for the Claim cannot be blank!");
            return;            
        }
            Organization audOrg = null;
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                if (organization instanceof AuditorOrganization) {
                    audOrg = organization;
                    break;
                }
            }
            if (audOrg != null) {
                              
                Remark rem = new Remark();
                rem.setRole("Supervisor");
                rem.setUsername(userAccount.getUsername());
                rem.setComment(supervisorRemrks.getText());
                rem.setStatus("SENDAUD");
                rem.setCreationDate(new Date());
                request.getRemarkList().add(rem);
                request.setStatus("Assigned to Auditor");
                audOrg.getWorkQueue().getWorkRequestList().add(request);
                userAccount.getWorkQueue().getWorkRequestList().remove(request);
                organization.getWorkQueue().getWorkRequestList().remove(request);
                
                for (UserAccount ua : audOrg.getUserAccountDirectory().getUserAccountList()) {
                    if (ua.getUsername().contentEquals(request.getAuditorID())){
                        ua.getWorkQueue().getWorkRequestList().add(request);                        
                        JOptionPane.showMessageDialog(null,"Claim assigned to : " +request.getAuditorID()+ " successfully" );
                        sendAuditor.setEnabled(false);
                        sendBackbtn.setEnabled(false);
                        saveBtn.setEnabled(false);
                        break;
                    }
                }
            }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting ProcessClaimJPanel Supervisor sendAuditorActionPerformed");
    }//GEN-LAST:event_sendAuditorActionPerformed

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        // TODO add your handling code here:
         try{
             
         log.debug("Entering ProcessClaimJPanel Supervisor saveBtnActionPerformed");                                
        if (supervisorRemrks.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Remarks for the Claim cannot be blank!");
            return;
        } else {
            Remark rem = new Remark();
            rem.setRole("Supervisor");
            rem.setUsername(userAccount.getUsername());
            rem.setComment(supervisorRemrks.getText());
            rem.setStatus("SAVE");
            rem.setCreationDate(new Date());
            request.getRemarkList().add(rem);
            JOptionPane.showMessageDialog(this, "Remarks saved successfully!");
            sendAuditor.setEnabled(false);
            sendBackbtn.setEnabled(false);
            saveBtn.setEnabled(false);
        }  
         }
         catch(Exception e)
         {
             log.error(e.getMessage());
         }
        log.debug("Exiting ProcessClaimJPanel Supervisor saveBtnActionPerformed");                                
    }//GEN-LAST:event_saveBtnActionPerformed

    private void sendBackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendBackbtnActionPerformed
        // TODO add your handling code here:
        try{
        log.debug("Entering ProcessClaimJPanel Supervisor sendBackbtnActionPerformed");                                
        if (supervisorRemrks.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Supervisor Remarks for the Claim cannot be blank!");
            return;            
        }
            Organization agnOrg = null;
            for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                if (organization instanceof AgentOrganization) {
                    agnOrg = organization;
                    break;
                }
            }
            if (agnOrg != null) {               
                Remark rem = new Remark();
                rem.setRole("Supervisor");
                rem.setUsername(userAccount.getUsername());
                rem.setComment(supervisorRemrks.getText());
                rem.setStatus("SAVE");
                rem.setCreationDate(new Date());
                request.getRemarkList().add(rem);
                request.setStatus("Assigned to Agent");
                agnOrg.getWorkQueue().getWorkRequestList().add(request);
                userAccount.getWorkQueue().getWorkRequestList().remove(request);
                organization.getWorkQueue().getWorkRequestList().remove(request);
                
                for (UserAccount ua : agnOrg.getUserAccountDirectory().getUserAccountList()) {
                    if (ua.getUsername().contentEquals(request.getAgentID())){
                        ua.getWorkQueue().getWorkRequestList().add(request);                        
                        JOptionPane.showMessageDialog(null,"Claim assigned back to : " +request.getAgentID()+ " successfully" );
                        sendAuditor.setEnabled(false);
                        sendBackbtn.setEnabled(false);
                        saveBtn.setEnabled(false);                       
                        break;
                    }
                }
            }
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        log.debug("Exiting ProcessClaimJPanel Supervisor sendBackbtnActionPerformed");                                
    }//GEN-LAST:event_sendBackbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField agentIDTextField;
    private javax.swing.JTextArea agentRemarksTextField;
    private javax.swing.JTextField agingTextField;
    private javax.swing.JButton btnBack;
    private javax.swing.JTextField claiDateTextField;
    private javax.swing.JTextField claimAmntTextField;
    private javax.swing.JTextField claimIDTextField;
    private javax.swing.JTextField claimSatusTextField;
    private javax.swing.JTextField hosptalIDTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextField policyStatusTextField;
    private javax.swing.JButton saveBtn;
    private javax.swing.JButton sendAuditor;
    private javax.swing.JButton sendBackbtn;
    private javax.swing.JTextArea supervisorRemrks;
    private javax.swing.JLabel svBnkAccBalLbl;
    private javax.swing.JLabel svBnkNamLbl;
    private javax.swing.JLabel svBnkNamLbl4;
    private javax.swing.JLabel svBnkRoutngNumLbl;
    private javax.swing.JLabel svBnkRoutngNumLbl1;
    private javax.swing.JTable tblCommHist;
    private javax.swing.JTextField txtEndDate;
    private javax.swing.JTextField txtGrade;
    private javax.swing.JTextField txtPolHoldName;
    private javax.swing.JTextField txtPolicyID;
    private javax.swing.JTextField txtStartDate;
    // End of variables declaration//GEN-END:variables
}
